# RELEASE NOTES FOR KOHA 20.05.12
16 Jun 2021

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.05.12 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.05.12.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.05.12 is a bugfix/maintenance release.

It includes 4 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required






## Critical bugs fixed

### Architecture, internals, and plumbing

- [[28200]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28200) Net::Netmask 1.9104-2 requires constructor change for backwards compatibility

  >The code library Koha uses for working with IP addresses has dropped support for abbreviated values in recent releases.  This is to tighten up the default security of input value's and we have opted in Koha to follow this change through into our system preferences for the same reason.
  >
  >WARNING: `koha_trusted_proxies` and `ILS-DI:AuthorizedIPs` are both affected. Please check that you are not using abbreviated IP forms for either of these cases. Example: "10.10" is much less explicit than "10.10.0.0/16" and should be avoided.

### I18N/L10N

- [[28419]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28419) Page addorderiso2709.pl is untranslatable

### Packaging

- [[28364]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28364) koha-z3950-responder breaks because of log4perl.conf permissions

### Searching

- [[28475]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28475) Searching all headings returns no results

  **Sponsored by** *Asociación Latinoamericana de Integración*




## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.05/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (98.6%)
- Armenian (100%)
- Armenian (Classical) (99.7%)
- Chinese (Taiwan) (94.2%)
- Czech (80.8%)
- English (New Zealand) (66.6%)
- English (USA)
- Finnish (70.4%)
- French (86.5%)
- French (Canada) (97.2%)
- German (100%)
- German (Switzerland) (74.4%)
- Greek (62.1%)
- Hindi (100%)
- Italian (100%)
- Norwegian Bokmål (71%)
- Polish (79.5%)
- Portuguese (86.6%)
- Portuguese (Brazil) (97.9%)
- Russian (86.5%)
- Slovak (89.6%)
- Spanish (100%)
- Swedish (79.5%)
- Telugu (100%)
- Turkish (100%)
- Ukrainian (66.3%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.05.12 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits
We thank the following libraries who are known to have sponsored
new features in Koha 20.05.12:

- Asociación Latinoamericana de Integración

We thank the following individuals who contributed patches to Koha 20.05.12.

- Tomás Cohen Arazi (2)
- David Cook (1)
- Jonathan Druart (2)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.05.12

- Koha Community Developers (2)
- Prosentient Systems (1)
- Theke Solutions (2)

We also especially thank the following individuals who tested patches
for Koha.

- Nick Clemens (1)
- Alvaro Cornejo (1)
- Jonathan Druart (3)
- Lucas Gass (1)
- Victor Grousset (7)
- Ere Maijala (1)
- Martin Renvoize (4)
- Fridolin Somers (5)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.05.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 16 Jun 2021 13:10:06.
