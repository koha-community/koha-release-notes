# RELEASE NOTES FOR KOHA 24.05.06
24 Feb 2025

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](https://koha-community.org)

Koha 24.05.06 can be downloaded from:

- [Download](https://download.koha-community.org/koha-24.05.06.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](https://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 24.05.06 is a bugfix/maintenance release.

It includes 3 bugfixes.

**System requirements**

You can learn about the system components (like OS and database) needed for running Koha on the [community wiki](https://wiki.koha-community.org/wiki/System_requirements_and_recommendations).


## Bugfixes

### ERM

#### Other bugs fixed

- [37810](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=37810) Some SUSHI providers return ServiceActive instead of Service_Active

### Installation and upgrade (command-line installer)

#### Critical bugs fixed

- [38779](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=38779) Record sources not working on packages install
  >This fixes the record sources page (Administration > Cataloging > Record sources) for package installs - you can now add and edit record sources, instead of getting a blank page. The Koha packages were missing the required JavaScript files (/intranet-tmpl/prog/js/vue/dist/admin/record_sources_24.1100000.js"></script>) to make the page work correctly.

### Searching - Elasticsearch

#### Critical bugs fixed

- [38913](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=38913) Elasticsearch indexing explodes with some oversized records with UTF-8 characters

## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha
documentation is

- [Koha Documentation](https://koha-community.org/documentation/)
As of the date of these release notes, the Koha manual is available in the following languages:

- [Armenian (hy_ARMN)](https://koha-community.org/manual/24.05//html/) (58%)
- [Bulgarian (bg_CYRL)](https://koha-community.org/manual/24.05//html/) (98%)
- [Chinese (Traditional)](https://koha-community.org/manual/24.05/zh_Hant/html/) (99%)
- [English](https://koha-community.org/manual/24.05//html/) (100%)
- [English (USA)](https://koha-community.org/manual/24.05/en/html/)
- [French](https://koha-community.org/manual/24.05/fr/html/) (58%)
- [German](https://koha-community.org/manual/24.05/de/html/) (100%)
- [Greek](https://koha-community.org/manual/24.05//html/) (92%)
- [Hindi](https://koha-community.org/manual/24.05/hi/html/) (71%)

The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:
<div style="column-count: 2;">

- Arabic (ar_ARAB) (97%)
- Armenian (hy_ARMN) (100%)
- Bulgarian (bg_CYRL) (100%)
- Chinese (Simplified) (88%)
- Chinese (Traditional) (99%)
- Czech (68%)
- Dutch (87%)
- English (100%)
- English (New Zealand) (63%)
- English (USA)
- Finnish (99%)
- French (99%)
- French (Canada) (99%)
- German (100%)
- Greek (65%)
- Hindi (98%)
- Italian (82%)
- Norwegian Bokmål (75%)
- Persian (fa_ARAB) (97%)
- Polish (100%)
- Portuguese (Brazil) (99%)
- Portuguese (Portugal) (88%)
- Russian (94%)
- Slovak (61%)
- Spanish (100%)
- Swedish (87%)
- Telugu (69%)
- Tetum (52%)
- Turkish (84%)
- Ukrainian (72%)
- hyw_ARMN (generated) (hyw_ARMN) (63%)
</div>

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](https://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](https://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](https://translate.koha-community.org/)

## Release Team

The release team for Koha 24.05.06 is


- Release Manager: Katrin Fischer

- Release Manager assistants:
  - Tomás Cohen Arazi
  - Martin Renvoize
  - Jonathan Druart

- QA Manager: Martin Renvoize

- QA Team:
  - Marcel de Rooy
  - Kyle M Hall
  - Emily Lamancusa
  - Nick Clemens
  - Lucas Gass
  - Tomás Cohen Arazi
  - Julian Maurice
  - Victor Grousset
  - Aleisha Amohia
  - David Cook
  - Laura Escamilla
  - Jonathan Druart
  - Pedro Amorim
  - Matt Blenkinsop
  - Thomas Klausner

- Topic Experts:
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - ERM -- Matt Blenkinsop
  - ILL -- Pedro Amorim
  - SIP2 -- Matthias Meusburger
  - CAS -- Matthias Meusburger

- Bug Wranglers:
  - Aleisha Amohia
  - Jacob O'Mara

- Packaging Managers:
  - Mason James
  - Tomás Cohen Arazi

- Documentation Manager: Philip Orr

- Documentation Team:
  - Aude Charillon
  - Caroline Cyr La Rose
  - Lucy Vaux-Harvey
  - Emmanuel Bétemps
  - Marie-Luce Laflamme
  - Kelly McElligott
  - Rasa Šatinskienė
  - Heather Hernandez

- Wiki curators: 
  - Thomas Dukleth
  - George Williams

- Release Maintainers:
  - 24.05 -- Lucas Gass
  - 23.11 -- Fridolin Somers
  - 23.05 -- Wainui Witika-Park
  - 22.11 -- Fridolin Somers

## Credits



We thank the following individuals who contributed patches to Koha 24.05.06
<div style="column-count: 2;">

- Pedro Amorim (2)
- Tomás Cohen Arazi (1)
- Nick Clemens (1)
- David Cook (1)
- Janusz Kaczmarek (1)
</div>

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 24.05.06
<div style="column-count: 2;">

- [ByWater Solutions](https://bywatersolutions.com) (1)
- Independant Individuals (1)
- [Prosentient Systems](https://www.prosentient.com.au) (1)
- [PTFS Europe](https://ptfs-europe.com) (2)
- [Theke Solutions](https://theke.io) (1)
</div>

We also especially thank the following individuals who tested patches
for Koha
<div style="column-count: 2;">

- Alex Buckley (6)
- Nick Clemens (2)
- David Cook (1)
- Paul Derscheid (3)
- Magnus Enger (1)
- Katrin Fischer (3)
- David Nind (3)
- Martin Renvoize (3)
</div>





We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to koha-devel@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development
version of Koha can be retrieved by checking out the main branch of:

- [Koha Git Repository](https://git.koha-community.org/koha-community/koha)

The branch for this version of Koha and future bugfixes in this release
line is 24.05.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](https://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 24 Feb 2025 16:26:07.
