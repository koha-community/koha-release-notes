# RELEASE NOTES FOR KOHA 20.11.00
18 Dec 2020

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.11.00 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.11-latest.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.11.00 is a major release, that comes with many new features.

It includes 17 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required






## Critical bugs fixed

(This list includes all bugfixes since the previous major version. Most of them
have already been fixed in maintainance releases)

### Database

- [[24658]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24658) Deleting items with fines does not update itemnumber in accountlines to NULL causing ISE
- [[27003]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27003) action_logs table error when adding an item

### Patrons

- [[27144]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27144) Cannot delete any patrons

### Reports

- [[27142]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27142) Patron batch update from report module - no patrons loaded into view

  >This fixes an error when batch modifying patrons using the reports module. After running a report (such as SELECT * FROM borrowers LIMIT 50) and selecting batch modification an error was displayed: "Warning, the following cardnumbers were not found:", and you were not able to modify any patrons.

### SIP2

- [[27166]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27166) SIP2 Connection is killed when an item that was not issued is checked in and generates a transfer

### Searching - Zebra

- [[12430]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=12430) Relevance ranking should also be used without QueryWeightFields system preference

### Web services

- [[26665]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26665) OAI 'Set' and 'Metadata' dropdowns broken

  >With OAI-PMH enabled, if you clicked on Sets or Metadata in the search results no additional information was displayed (example query: <OPACBaseURL>/cgi-bin/koha/oai.pl?verb=ListRecords&metadataPrefix=marc21). This patch fixes this so that the additional information for Sets and Metadata is now correctly displayed.


## Other bugs fixed

(This list includes all bugfixes since the previous major version. Most of them
have already been fixed in maintainance releases)

### Acquisitions

- [[26905]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26905) Purchase suggestion button hidden for users with suggestion permission but not acq permission

### Architecture, internals, and plumbing

- [[16067]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16067) Koha::Cache, fastmmap caching system is broken
- [[27030]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27030) The new "Processing" hold status is missing in C4::Reserves module documentation

### Cataloging

- [[27128]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27128) Follow-up to bug 25728 - Don't prefill av's code

### Circulation

- [[25583]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25583) When ClaimReturnedLostValue is not set, the claim returned tab doesn't appear
- [[27133]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27133) Header missing for "Copy no" on the relative's checkouts table

### Hold requests

- [[26976]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26976) When renewalsallowed is empty the UI is not correct

### Staff Client

- [[23475]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23475) Search context is lost when simple search leads to a single record
- [[26946]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26946) Limit size of cash register's name on the UI

### Tools

- [[26336]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26336) Cannot import items if items ignored when staging


## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.11/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (89.2%)
- Armenian (100%)
- Armenian (Classical) (89%)
- Chinese (Taiwan) (85.7%)
- Czech (73.6%)
- English (New Zealand) (60%)
- English (USA)
- Finnish (78.8%)
- French (73.5%)
- French (Canada) (91.4%)
- German (100%)
- German (Switzerland) (67.5%)
- Greek (57.5%)
- Hindi (91.3%)
- Italian (100%)
- Norwegian Bokmål (63.9%)
- Polish (67.5%)
- Portuguese (77.9%)
- Portuguese (Brazil) (88.7%)
- Slovak (80.9%)
- Spanish (94.9%)
- Swedish (75.3%)
- Telugu (80.3%)
- Turkish (87%)
- Ukrainian (61.7%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.11.00 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits

We thank the following individuals who contributed patches to Koha 20.11.00.

- Nick Clemens (6)
- David Cook (2)
- Jonathan Druart (13)
- Joonas Kylmälä (1)
- Owen Leonard (1)
- Josef Moravec (1)
- Martin Renvoize (1)
- Fridolin Somers (1)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.11.00

- Athens County Public Libraries (1)
- BibLibre (1)
- ByWater-Solutions (6)
- Independant Individuals (1)
- Koha Community Developers (13)
- Prosentient Systems (2)
- PTFS-Europe (1)
- University of Helsinki (1)

We also especially thank the following individuals who tested patches
for Koha.

- Nick Clemens (2)
- Jonathan Druart (14)
- Katrin Fischer (1)
- Lucas Gass (1)
- Victor Grousset (9)
- Kyle M Hall (3)
- Sally Healey (1)
- Joonas Kylmälä (1)
- Julian Maurice (3)
- Kelly McElligott (1)
- Josef Moravec (1)
- David Nind (5)
- Martin Renvoize (8)
- Fridolin Somers (19)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.11.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 18 Dec 2020 16:58:10.
