# RELEASE NOTES FOR KOHA 20.11.04
22 Apr 2021

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.11.04 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.11.04.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.11.04 is a bugfix/maintenance release.

It includes 7 enhancements, 43 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required




## Enhancements

### Command-line Utilities

- [[26459]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26459) Allow sip_cli_emulator to handle cancelling holds
- [[27839]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27839) koha-worker missing tab-completion in bash

### OPAC

- [[27991]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27991) Message field for checkout notes should have a maxlength set

### Templates

- [[26970]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26970) Add row highlight on drag in Elasticsearch mapping template
- [[28006]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28006) Restore "Additional fields" link on serials navigation menu
- [[28046]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28046) Add "Additional fields" link on acquisition navigation menu
- [[28132]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28132) Remove "this" from button descriptions on basket and basket group pages


## Critical bugs fixed

### Architecture, internals, and plumbing

- [[26705]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26705) System preference NoticeBcc not working

  >The Email::Stuffer library we use, doesn't handle Bcc as Mail::Sendmail does. So Bcc handling wasn't working as expected. This patchset adds support for explicitly handling Bcc (including the NoticeBcc feature) to our Koha::Email library.

### Circulation

- [[28136]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28136) Transferred holds are not triggering

### Notices

- [[28023]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28023) Typo in Reply-To header

### Patrons

- [[25946]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25946) borrowerRelationship can no longer be empty
- [[26517]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26517) Avoid deleting patrons with permission

### Searching - Elasticsearch

- [[26312]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26312) Add some error handling during Elasticsearch indexing


## Other bugs fixed

### Acquisitions

- [[27900]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27900) regression: add from existing record with null results deadends
- [[28003]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28003) Invoice adjustments using inactive budgets do not indicate that status
- [[28077]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28077) Missing colon on suggestion modification page

### Architecture, internals, and plumbing

- [[24000]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24000) Some modules do not return 1
- [[27807]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27807) API DataTables Wrapper fails for ordered on multiple columns
- [[28053]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28053) Warning in C4::Members::patronflags

### Cataloging

- [[27738]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27738) Set fallback for unset DefaultCountryField008 to |||, "no attempt to code"

### Circulation

- [[27969]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27969) On checkin, relabel "Remember due date" as "Remember return date"

### Fines and fees

- [[28097]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28097) t/db_dependent/Koha/Account/Line.t test fails with FinesMode set to calculate

### Hold requests

- [[26999]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26999) "Any library" not translatable on the hold list
- [[27921]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27921) Timestamp in holds log is out of date when a hold is marked as waiting

### Notices

- [[28017]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28017) Allow non-FQDN and IP addresses in emails

### OPAC

- [[27726]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27726) OPACProblemReports cuts off message text
- [[27748]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27748) Encoding problem in link to OverDrive results
- [[27881]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27881) Markup error in masthead-langmenu.inc
- [[27889]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27889) Form fields in OPAC are "out of shape"

  >This patch tweaks the CSS for the advanced search form in the OPAC so that it adjusts well at various browser widths, including preventing the form from taking up the whole width of the page at higher browser widths.
- [[27940]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27940) Fix missing email in OpacMaintenance page
- [[28094]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28094) Fix bad encoding of OVERRIDE_SYSPREF_LibraryName

### Patrons

- [[27937]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27937) Date of birth entered  without correct format causes internal server error
- [[28043]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28043) Some patron clubs operations don't work from later pages of results

### SIP2

- [[27936]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27936) AllowItemsOnHoldCheckoutSIP does not allow checkout of items currently waiting for a hold
- [[28052]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28052) keys omitted in check for system preference override
- [[28054]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28054) SIPServer.pm is a program and requires a shebang

### Searching

- [[27746]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27746) Use of uninitialized value $oclc in pattern match (m//) error at C4/Koha.pm
- [[27928]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27928) FindDuplicate is hardcoded to use Zebra
- [[28074]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28074) Browse controls on staff detail pages are sometimes weird

### Serials

- [[27397]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27397) Serials: The description input field when defining numbering patterns is too short

### Staff Client

- [[27926]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27926) Date of birth sorting with British English format is broken

### System Administration

- [[27999]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27999) Display the description of authorized values category
- [[28121]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28121) Wrong punctuation on desk deletion

### Templates

- [[28004]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28004) Incomplete breadcrumbs in authorized valued

  >This patch fixes some incorrect displays within the breadcrumbs on authorised_values.tt
- [[28032]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28032) Button corrections in point of sale pages
- [[28042]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28042) Button corrections in OAI set mappings template

### Tools

- [[26942]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26942) TinyMCE in the News Tool is still doing some types of automatic code cleanup
- [[27963]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27963) touch_all_items.pl script is not working at all
- [[28044]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28044) Calendar: Tables with closed days are no longer color coded

### Z39.50 / SRU / OpenSearch Servers

- [[28112]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28112) Z39.50 does not populate form with all passed criteria


## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.11/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (99.6%)
- Armenian (100%)
- Armenian (Classical) (89%)
- Catalan; Valencian (52.1%)
- Chinese (Taiwan) (89.2%)
- Czech (73.1%)
- English (New Zealand) (59.6%)
- English (USA)
- Finnish (78.2%)
- French (88.1%)
- French (Canada) (91.2%)
- German (100%)
- German (Switzerland) (67%)
- Greek (60.8%)
- Hindi (100%)
- Italian (100%)
- Norwegian Bokmål (63.5%)
- Polish (81.8%)
- Portuguese (77.3%)
- Portuguese (Brazil) (94.2%)
- Russian (94.2%)
- Slovak (80.8%)
- Spanish (98.7%)
- Swedish (74.6%)
- Telugu (79.6%)
- Turkish (96.6%)
- Ukrainian (64.3%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.11.04 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits

We thank the following individuals who contributed patches to Koha 20.11.04.

- Tomás Cohen Arazi (13)
- Colin Campbell (2)
- Nick Clemens (8)
- Jonathan Druart (7)
- Magnus Enger (2)
- Katrin Fischer (2)
- Andrew Fuerste-Henry (1)
- Lucas Gass (3)
- Joonas Kylmälä (1)
- Owen Leonard (14)
- Catherine Ma (1)
- James O'Keeffe (1)
- Séverine Queune (2)
- Martin Renvoize (9)
- Phil Ringnalda (1)
- Marcel de Rooy (1)
- Fridolin Somers (4)
- Petro Vashchuk (4)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.11.04

- Athens County Public Libraries (14)
- BibLibre (4)
- Bibliothèque Universitaire des Langues et Civilisations (BULAC) (2)
- BSZ BW (2)
- ByWater-Solutions (12)
- Chetco Community Public Library (1)
- Independant Individuals (6)
- Koha Community Developers (7)
- Libriotech (2)
- PTFS-Europe (11)
- Rijks Museum (1)
- Theke Solutions (13)
- University of Helsinki (1)

We also especially thank the following individuals who tested patches
for Koha.

- Tomás Cohen Arazi (2)
- Nick Clemens (4)
- Donna (1)
- Jonathan Druart (74)
- Katrin Fischer (22)
- Andrew Fuerste-Henry (4)
- Lucas Gass (2)
- Victor Grousset (2)
- Amit Gupta (9)
- Kyle M Hall (9)
- Sally Healey (1)
- Joonas Kylmälä (10)
- Owen Leonard (7)
- Julian Maurice (2)
- Kelly McElligott (1)
- David Nind (13)
- Séverine QUEUNE (1)
- Séverine Queune (3)
- Martin Renvoize (26)
- Phil Ringnalda (1)
- Fridolin Somers (73)
- Petro Vashchuk (2)

We thank the following individuals who mentored new contributors to the Koha project.

- Andrew Nugged


We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.11.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 22 Apr 2021 11:00:40.
