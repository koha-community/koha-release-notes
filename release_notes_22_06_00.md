# RELEASE NOTES FOR KOHA 22.06.00
10 Mar 2023

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 22.06.00 can be downloaded from:

- [Download](http://download.koha-community.org/koha-22.11-latest.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 22.06.00 is a major release, that comes with many new features.

It includes 2 security fixes, 5 new features, 67 enhancements, 296 bugfixes.

### System requirements

You can learn about the system components (like OS and database) needed for running Koha [here](https://wiki.koha-community.org/wiki/System_requirements_and_recommendations).


## Security bugs

### Koha

- [[31908]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31908) New login fails while having cookie from previous session

  >This patch introduces more thorough cleanup of user sessions when logging after a privilege escalation request.
- [[32208]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32208) Relogin without enough permissions needs attention

## New features

### OPAC

- [[31028]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31028) Add 'Report a concern' feature for patrons to report concerns about catalog records
- [[31051]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31051) Show patron's 'savings' on the OPAC

  **Sponsored by** *Horowhenua Libraries Trust*

  >This new feature shows a patron how much they have saved by using the library rather than purchasing items. Savings are calculated based on item replacement prices. The system preference allows you to choose where to display the savings - the user page, the summary box on the OPAC homepage, or the checkout history page.

### REST API

- [[31793]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31793) REST API: DELETE endpoint for Authorities

  >This adds a DELETE endpoint for authorities to the REST API.
- [[31794]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31794) REST API: GET endpoint for Authorities

  >This adds a GET endpoint for authorities to the REST API.
- [[31797]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31797) REST API: DELETE endpoint for Items

## Enhancements

### Acquisitions

- [[25655]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25655) Additionally store actual cost in foreign currency and currency from the invoice

  **Sponsored by** *The Research University in the Helmholtz Association (KIT)*
- [[29935]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=29935) Add option to search in archive to all information tabs
- [[32452]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32452) Link basket group name from basket summary page
- [[32705]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32705) Display actual cost in foreign currency and currency from the invoice

  **Sponsored by** *The Research University in the Helmholtz Association (KIT)*
- [[32992]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32992) Move background worker script to misc/workers

### Architecture, internals, and plumbing

- [[30310]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30310) Replace Moment.js with Day.js
- [[30642]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30642) We should record the renewal type (automatic/manual)
- [[31095]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31095) Remove Koha::Patron::Debarment::GetDebarments and use $patron->restrictions in preference
- [[32609]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32609) Remove compiled files from src
- [[32806]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32806) Some Vue files need to be moved for better reusability

### Cataloging

- [[20256]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20256) Add ability to limit editing of items to home library or library group
- [[23656]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23656) Add search box at the top of the cataloging editor page

### Circulation

- [[30963]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30963) Automatically refresh the curbside pickups list

  **Sponsored by** *Association KohaLa*
- [[32134]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32134) Show the bundle size when checked out

### Command-line Utilities

- [[30069]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30069) Add edifact_messages to cleanup_database.pl

  **Sponsored by** *PTFS Europe*
- [[31453]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31453) Add ability to filter messages to process using process_message_queue.pl via a command line parameter
- [[32518]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32518) Add reason option to cancel_unfilled_holds

### ERM

- [[32495]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32495) Required fields in API and UI form don't match

  >This enhancement changes the new agreement form so that the description field is no longer required (to match with the API).
- [[32924]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32924) Filter agreements by logged in librarian
- [[32925]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32925) Display loading info when a form is submitted
- [[32939]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32939) Have generic fetch functions in vue modules
- [[32983]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32983) Use REST API route to retrieve authorised values
- [[32991]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32991) Improve our Dialog component and remove routes for deletion

### Fines and fees

- [[32977]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32977) Add call number column to list of charges on transactions tab in patron account

### ILL

- [[32546]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32546) Move ILL system preferences to their own tab in administration

### Lists

- [[32434]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32434) Records in lists not showing what other lists they belong to

### Notices

- [[29100]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=29100) Add checkouts data loop to predue/due notices script (advance_notices.pl)
- [[31858]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31858) TT syntax for ACQORDER notices

### OPAC

- [[26765]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26765) Make author span a clickable link on OPAC results list
- [[31221]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31221) Buttons overflow in OPAC search results in mobile view
- [[31699]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31699) Add a generic way to redirect back to the page you were on at login for modal logins

  >This enhancement adds the ability to redirect users back to where they were when using the modal type logins in place of an action that requires login on the OPAC.
  >
  >Example: On the OPAC detail page you can add comments if logged in. Prior to this patch, clicking the link to add a comment prior to being logged in would expose the login modal and then re-direct you to your OPAC user page, and thus lose the context of your action.  With this enhancement, you are redirected back to the record you were looking at and can then post your comment.
- [[32125]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32125) Implement contextual return on OPAC comments

  >This enhancement ensures patrons are returned to the correct biblio detail page after a login that is prompted when attempting to comment on a bilio.
- [[33031]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33031) Update OPAC lists page to use Bootstrap markup for tabs

### REST API

- [[30962]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30962) REST API: Add endpoint /auth/password/validation
- [[32981]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32981) Add REST API endpoint to list authorised values for a given category
- [[32997]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32997) Add REST API endpoint to list authorised values for multiple given categories
- [[33161]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33161) Implement +strings for GET /items and /items/:item_id

  **Sponsored by** *Virginia Polytechnic Institute and State University*

### Reports

- [[17350]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17350) Add option to delete data stored in saved_reports with cleanup_database
- [[32613]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32613) Can we add 'autocompletion' to our SQL reports editor?

### SIP2

- [[25812]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25812) Fines can be displayed on SIP checkin/checkout
- [[32431]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32431) Show date for expired patrons in SIP

### Searching

- [[14911]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14911) Item search: Display additional 245 subfields in search results
- [[31338]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31338) Show in advanced search when IncludeSeeFromInSearches is used
- [[32960]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32960) Add option in item search for excluding checked out items

### Self checkout

- [[32115]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32115) Add ID to check-out default help message dialog to allow customization

### Staff interface

- [[32173]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32173) Add count of total titles in list to staff client view
- [[32886]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32886) Set focus for cursor to Code when adding a new restriction
- [[33090]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33090) page-sections are missing in the account line details page

### System Administration

- [[27424]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27424) One should be able to assign an SMTP server as the default

### Templates

- [[31407]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31407) Set focus for cursor to Currency when adding a new currency
- [[32095]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32095) Remove bullets from statuses in inventory tool
- [[32319]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32319) Give header search submit button more padding
- [[32507]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32507) Use template wrapper to build breadcrumb navigation

  >Architectural enhancement in preparation for bootstrap 5 upgrade.  This patch adds the foundations for abstracting the breadcrumb component of the staff client.
- [[32571]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32571) Use template wrapper to build tabbed components
- [[32649]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32649) Use template wrapper for library transfer limits tabs
- [[32658]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32658) Use template wrapper in order from staged file template
- [[32660]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32660) Use template wrapper for basket groups tabs
- [[32661]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32661) Use template wrapper for invoices page tabs
- [[32662]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32662) Use template wrapper for item circulation alerts page
- [[32688]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32688) Convert recalls awaiting pickup tabs to Bootstrap
- [[32698]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32698) Use template wrapper for serials pages tabs
- [[32746]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32746) Standardize structure around action fieldsets in acquisitions
- [[32769]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32769) Standardize structure around action fieldsets in administration
- [[33000]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33000) Use template wrapper for breadcrumbs: Acquisitions part 1
- [[33068]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33068) Use template wrapper for breadcrumbs: Administration part 3
- [[33071]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33071) Show tooltip when hovering on home icon in staff interface breadcrumbs

### Tools

- [[32019]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32019) Add option to mark items returned in batch modification


## Critical bugs fixed

(This list includes all bugfixes since the previous major version. Most of them
have already been fixed in maintainance releases)

### Acquisitions

- [[32401]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32401) x-koha-query cannot contain non-ISO-8859-1 values

### Architecture, internals, and plumbing

- [[32393]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32393) background job worker explodes if JSON is incorrect
- [[32394]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32394) Long tasks queue is never used
- [[32422]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32422) Hardcoded paths in _common.scss prevent using external node_modules
- [[32472]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32472) [21.11 CRASH] The method Koha::Item->count is not covered by tests
- [[32481]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32481) Rabbit times out when too many jobs are queued and the response takes too long
- [[32558]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32558) Allow background_jobs_worker.pl to process multiple jobs simultaneously up to a limit
- [[32561]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32561) background job worker is still running with all the modules in RAM
- [[32612]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32612) Koha background worker should log to worker-error/output.log
- [[32656]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32656) Script delete_records_via_leader.pl no longer deletes items
- [[33044]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33044) BackgroundJob enqueue does not return the job id if rabbit is unreachable

### Authentication

- [[32354]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32354) Handle session_state param given by OAuth identity provider

  **Sponsored by** *The New Zealand Institute for Plant and Food Research Limited*

  >This patch ensures Koha doesn't throw an error if the IdP hands back a session_state parameter.

### Cataloging

- [[32550]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32550) 'Clear on loan' link on Batch item modification doesn't untick on loan items

### Circulation

- [[32653]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32653) Curbside pickups - wrong dates available at the OPAC
- [[32891]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32891) Curbside pickups - Cannot select slot in the last hour

### Command-line Utilities

- [[32798]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32798) build_oai_sets.pl passes wrong parameter to Koha::Biblio::Metadata->record

### ERM

- [[32468]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32468) Vendors select only allows selecting from first 20 vendors by default
- [[32779]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32779) Import from list is broken

### Fines and fees

- [[30254]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30254) New overdue fine applied to incorrectly when using "Refund lost item charge and charge new overdue fine" option in circ rules

### Hold requests

- [[32470]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32470) (Bug 14783 follow-up) Fix mysql error in db_rev for 22.06.000.064

### I18N/L10N

- [[32356]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32356) xx-XX installer dir /kohadevbox/koha/installer/data/mysql/xx-XX already exists.

### Installation and upgrade (web-based installer)

- [[32399]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32399) Database update for bug 30483 is failing

### Notices

- [[32442]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32442) Invalid Template Toolkit in notices can cause errors

### OPAC

- [[32445]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32445) Status display of 'not for loan' items is broken in OPAC/staff
- [[32674]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32674) When placing a hold in OPAC page explodes into error 500
- [[32712]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32712) OPACShowCheckoutName makes OPAC explode
- [[33101]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33101) Basket More details view doesn't work

### Packaging

- [[32994]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32994) Remove compiled files from src (2)

### Plugin architecture

- [[32539]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32539) UI hooks can break the UI

### REST API

- [[31381]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31381) Searching patrons by letter broken when using non-mandatory extended attributes
- [[33020]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33020) Unsupported method history
- [[33145]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33145) Invalid specification for ERM routes

### SIP2

- [[32515]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32515) SIP2 no block flag on checkin calls routine that does not exist
- [[33055]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33055) SIP2 adding incorrect fines blocked message

### Searching

- [[32126]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32126) Adding item search fields is broken - can't add more than one field

### Searching - Elasticsearch

- [[32594]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32594) Add a dedicated ES indexing background worker

### Serials

- [[32555]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32555) Error when viewing serial in OPAC
- [[33014]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33014) Add link to serial advanced search

### Staff interface

- [[31935]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31935) Serials subscription form is misaligned

  >This fixes the alignment of the serials subscription form.
- [[32517]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32517) Patron search dies on case mismatch of patron category

  >This fixes patron search so that searching by category will work regardless of the patron category code case (upper, lower, and sentence case). Before this, category codes in upper case were expected - where they weren't this caused the search to fail, resulting in no search results.
- [[32772]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32772) Patron autocomplete should not use contains on all fields

### Test Suite

- [[32898]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32898) Cypress tests are failing

### Tools

- [[32054]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32054) GetImportRecordMatches returns the wrong match when passed 'best only'
- [[32631]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32631) Error when previewing record during batch record modification

  >This patch corrects an error in the script which outputs MARC data for preview during batch record modification.


## Other bugs fixed

(This list includes all bugfixes since the previous major version. Most of them
have already been fixed in maintainance releases)

### About

- [[32665]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32665) warnPrefRequireChoosingExistingAuthority condition incorrect in about.pl
- [[32687]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32687) About may list version of SQL client in container, not actual server

### Acquisitions

- [[20473]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20473) "Item information" tab should not appear if item is not created upon placing an order
- [[31056]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31056) Unable to 'Close and export as PDF' a basket group
- [[31984]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31984) TaxRate system preference - add note about updating vendor tax rates where required

  >This enhancement adds a note to the TaxRates system preference about updating vendors tax rates when the TaxRates system preference values are changed or removed. (Vendors retain the original value entered, and this is used to calculate the tax rate for orders.)
- [[32377]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32377) GetBudgetHierarchy slows down acqui/histsearch.pl

  **Sponsored by** *Koha-Suomi Oy*
- [[32382]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32382) Fund input misaligned on invoice summary page
- [[32406]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32406) Cannot search pending orders using non-latin-1 scripts
- [[32417]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32417) Cannot insert order: Mandatory parameter biblionumber is missing
- [[32531]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32531) Filter 'Include archived' no longer shows non-archived suggestions
- [[32603]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32603) Suggester category in Suggestions management
- [[32694]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32694) Keep current option for budgets in receiving broken
- [[33002]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33002) 'Archive selected' button missing?
- [[33003]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33003) Show the vendor type description on vendor detail page when AV is used

### Architecture, internals, and plumbing

- [[18247]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18247) Remove SQL queries from branch_transfer_limit.pl administrative script

  **Sponsored by** *Catalyst*
- [[23247]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23247) Use EmbedItems in opac-MARCdetail.pl
- [[28672]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28672) Improve EDI debug logging
- [[31675]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31675) Remove packages from debian/control that are no longer used
- [[31893]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31893) Some pages load about.tt template to check authentication rather than using checkauth
- [[32330]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32330) Table background_jobs is missing indexes
- [[32457]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32457) CGI::param called in list context from acqui/addorder.pl line 182
- [[32460]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32460) Columns missing from table configuration for patron categories
- [[32465]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32465) koha-worker debian script missing 'queue' in help

  >This adds information about the --queue option to the help text for the koha-worker script.
- [[32528]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32528) Koha::Item->safe_to_delete should short-circuit earlier
- [[32529]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32529) Holds in processing should block item deletion
- [[32573]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32573) background_jobs_worker.pl should ACK a message before it forks and runs the job
- [[32580]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32580) Background job cancel button broken, leads to background_jobs.pl with a kc
- [[32582]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32582) Mailmap maps to wrong email address
- [[32583]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32583) Restore display of only one item in catalogue/moredetails
- [[32585]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32585) Followup on Bug 32583 - fix some variable references
- [[32678]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32678) Add new line in authorized values tests in search_for_data_inconsistencies.pl
- [[32811]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32811) Remove unused indexer.log
- [[32922]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32922) Remove space in shebang
- [[32935]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32935) basketgroup.js is not longer used and should be removed
- [[32975]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32975) Error in package.json's definition of css:build vs css:build:prod
- [[32978]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32978) 'npm install' fails in ktd on aarch64, giving unsupported architecture error for node-sass

### Cataloging

- [[3831]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=3831) Add a warning/hint when FA framework is missing
- [[15869]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15869) Change framework on overlay

  >This change fixes a long-standing bug where the framework specified during import only applied to new records and not overlaid matches.
- [[29173]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=29173) Button "replace authority record via Z39/50/SRU" doesn't pre-fill

  >This fixes the behaviour of the replace an authority record via Z39.50/SRU buttons when editing an authority record. Both ways of doing this (Edit > Edit record > Replace record via Z39.50/SRU search and Edit > Replace record via Z39.50/SRU search) now pre-fill the search form with available data.
- [[31665]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31665) 952$d ( Date acquired ) no longer prefills with todays date when focused
- [[32204]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32204) in-page anchor to edititem on additem.pl not working
- [[32321]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32321) 006 field not correctly prepopulated in Advanced cataloging editor
- [[32567]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32567) Update plugin unimarc_field_110.pl 'Script of title' and 'Transliteration code'
- [[32692]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32692) Terminology: MARC framework tag subfield editor uses intranet instead of staff interface
- [[32812]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32812) Fix cataloguing/value_builder/barcode_manual.pl
- [[32813]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32813) Fix cataloguing/value_builder/barcode.pl
- [[32814]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32814) Fix cataloguing/value_builder/callnumber-KU.pl
- [[32815]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32815) Fix cataloguing/value_builder/callnumber.pl
- [[32816]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32816) Fix cataloguing/value_builder/cn_browser.pl
- [[32819]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32819) Fix cataloguing/value_builder/stocknumberam123.pl
- [[32820]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32820) Fix cataloguing/value_builder/stocknumberAV.pl
- [[32821]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32821) Fix cataloguing/value_builder/stocknumber.pl
- [[32822]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32822) Fix cataloguing/value_builder/unimarc_field_010.pl
- [[32823]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32823) Fix cataloguing/value_builder/unimarc_field_100_authorities.pl
- [[32824]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32824) Fix cataloguing/value_builder/unimarc_field_100.pl
- [[32825]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32825) Fix cataloguing/value_builder/unimarc_field_105.pl
- [[32826]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32826) Fix cataloguing/value_builder/unimarc_field_106.pl
- [[32827]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32827) Fix cataloguing/value_builder/unimarc_field_110.pl
- [[32828]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32828) Fix cataloguing/value_builder/unimarc_field_115a.pl
- [[32829]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32829) Fix cataloguing/value_builder/unimarc_field_115b.pl
- [[32835]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32835) Fix cataloguing/value_builder/unimarc_field_122.pl
- [[33173]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33173) Save and continue button in standard cataloging module broken

### Circulation

- [[14784]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14784) Missing checkin message for debarred patrons when issuing rules 'fine days = 0'
- [[28975]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28975) Holds queue lists can show holds from all libraries even with IndependentBranches
- [[29021]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=29021) Automatic renewal due to RenewAccruingItemWhenPaid should not be considered Seen
- [[31209]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31209) Add a span with class around serial enumeration/chronology data in list of checkouts for better styling
- [[31233]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31233) Fine grace period in circulation conditions is misnamed
- [[31563]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31563) Numbers on claims tab not showing in translated templates
- [[32503]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32503) Holds awaiting pickup doesn't sort dates correctly

### Command-line Utilities

- [[32793]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32793) import_patrons.pl typo in usage

### Database

- [[28674]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28674) old_reserves.item_level_hold and reserves.item_level_hold comments have typo "hpld" not "hold"

### ERM

- [[32728]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32728) ERM - Search header should change to match the section you are in

### Fines and fees

- [[22042]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22042) BlockReturnofWithdrawn Items does not block refund generation when item is withdrawn and lost

### Hold requests

- [[32247]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32247) Real time HoldsQueue does not need to check items if there are no holds
- [[32455]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32455) Don't send hold notices from the library's inbound email address

### I18N/L10N

- [[22490]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22490) Some strings in JavaScript files are untranslatable
- [[30993]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30993) Translation: Unbreak sentence in upload.tt
- [[31957]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31957) Translation: Ability to change the sentence structure on library administration page
- [[32292]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32292) Update and add database column descriptions used in guided reports

  >This completes and adds column descriptions that show up when creating a new guided report for following tables:
  >* items
  >* borrowers
  >* biblio
  >* aqorders
  >* suggestions
- [[32588]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32588) Filters on top of 'Items with no checkouts' report are untranslatable

### ILL

- [[22693]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22693) ILL "Price paid" column does not appear in column configuration

  >This adds the "Price paid" column to the inter-library loan requests table.  This column is also configurable using the Columns button and in the table settings (Administration > Additional parameters > Table settings > Interlibrary loans > ill-requests).
- [[32525]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32525) Standardize labels between ILL request list and detail page
- [[32566]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32566) Don't show 'ILL request logs' button, when IllLog is turned off
- [[32799]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32799) ILLSTATUS authorized value category name is confusing

### Installation and upgrade (command-line installer)

- [[33051]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33051) 22600075.pl is not idempotent

### Installation and upgrade (web-based installer)

- [[32918]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32918) ERM authorized values should be in installer/data/mysql/en/mandatory/auth_values.yml

### Lists

- [[32302]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32302) "ISBN" label shows when no ISBN data present when sending list

  >This fixes email messages sent when sending lists so that if there are no ISBNs for a record, an empty label is not shown.

### MARC Bibliographic data support

- [[23032]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23032) Add 264 to Alternate Graphic Representation (MARC21 880)
- [[31860]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31860) Standardize capitalization for item subfield descriptions (UNIMARC 995/MARC21 952)
- [[32689]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32689) Host item entry (773) missing a space between label and content when $i is used

### Notices

- [[24616]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24616) Cannot copy notice to another library if it already exists

  **Sponsored by** *Koha-Suomi Oy*
- [[32221]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32221) Password entry should be removed from placeholder list in notices editor

### OPAC

- [[16522]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16522) Add 773 (Host item entry) to the cart and list displays and e-mails

  **Sponsored by** *Bibliotheksservice-Zentrum Baden-Württemberg (BSZ)*

  >This adds information from host item entry (MARC21 773) and if applicable a link to the host record in the following places:
  >* Staff interface: list, list email, cart, cart email, and search results
  >* OPAC: list, list email, cart, cart email, and search results
- [[31248]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31248) Fix responsive table style in the OPAC after switch to Bootstrap tabs
- [[32251]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32251) opac-page.pl: Add a fallback for when language cookie was removed
- [[32338]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32338) OPAC - Mobile - Selection toolbar in search result is shifted and not adjusted
- [[32492]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32492) Improve mark-up of OPAC messaging table to ease customization
- [[32597]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32597) Article requests not stacking in patron view
- [[32611]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32611) Not for loan items don't show the specific not for loan value in OPAC detail page
- [[32663]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32663) Street number should not allow for entering more than 10 characters in OPAC
- [[32679]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32679) CSS class article-request-title is doubled up in article requests list in staff patron account
- [[32946]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32946) 'Send list/cart' forms in OPAC is misaligned
- [[32999]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32999) Click handler to show QR code in OPAC lacks preventDefault

### Patrons

- [[31166]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31166) Digest option is not selectable for phone when PhoneNotification is enabled
- [[31492]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31492) Patron image upload fails on first attempt with CSRF failure
- [[31890]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31890) PrefillGuaranteeField should include option to prefill surname

  >This bugfix adds the surname field to the list of fields (in the PrefillGuaranteeField system preference) that can be automatically prefilled when adding a guarantee to a patron's account.
- [[32491]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32491) Can no longer search patrons in format 'surname, firstname'
- [[32505]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32505) Cannot search by dateofbirth in specified dateformat
- [[32570]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32570) City is duplicated in patron search if the patron has both city and state
- [[32655]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32655) Variables showing in patron messaging preferences
- [[32675]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32675) Cannot add a guarantor when there is a single quote in the guarantor attributes
- [[32770]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32770) Patron search field groups no longer exist
- [[32904]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32904) Patrons table processing eternally and not loading

  **Sponsored by** *Education Services Australia SCIS*
- [[33155]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33155) Category and library filters from header patron search not taken into account

### Plugin architecture

- [[33189]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33189) Plugin upload should prompt for .kpz files

### REST API

- [[31160]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31160) Required fields in the Patrons API are a bit random
- [[32409]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32409) Cannot search cashups using non-latin-1 scripts

  >This fixes the cashup history table so that filters can use non latin-1 characters (Point of sale > Cash summary for <library> > select register). Before this fix, the table was not filtered or refreshed if you entered non latin-1 characters.
- [[32923]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32923) x-koha-embed must a header of collectionFormat csv

### Reports

- [[32589]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32589) Improve headings on result tables for 'checkouts with no items' report
- [[32805]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32805) When recording localuse in the statistics table location is always NULL
- [[33063]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33063) Duplicated reports should maintain subgroup of original
- [[33152]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33152) Set focus for cursor to search input box when creating new report from "New SQL from Mana" option

### SIP2

- [[32408]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32408) If a fine can be overridden on checkout in Koha, what should the SIP client do?
- [[32537]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32537) Add 'no_block' option to sip_cli_emulator

  >This enhanced adds the no-block option to the SIP emulator for optional use in checkout/checkin/renew messages.
- [[32624]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32624) Patrons fines are not accurate in SIP2 when NoIssuesChargeGuarantorsWithGuarantees or NoIssuesChargeGuarantees are enabled

### Searching

- [[20596]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20596) Authority record matching rule causes staging failure when MARC record contains multiple tag values for a match point
- [[31326]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31326) Koha::Biblio->get_components_query fetches too many component parts

  **Sponsored by** *Koha-Suomi Oy*
- [[32639]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32639) OpenSearch description format document generates search errors

### Searching - Elasticsearch

- [[31471]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31471) Duplicate check in cataloguing fails with Elasticsearch for records with multiple ISBN

### Searching - Zebra

- [[32416]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32416) arp - Accelerated reader point searches fail due to conflicting attribute

  >This fixes
- [[32741]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32741) Attribute codes should not be repeated in bib1.att

### Self checkout

- [[19188]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19188) Self checkout: Fine blocking checkout is missing currency symbol

### Staff interface

- [[28314]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28314) Spinning icon is not always going away for local covers in staff
- [[31768]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31768) Tags is a 'Tool' but doesn't include the tools nav sidebar
- [[31950]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31950) Page section on library view is too wide / not aligned with toolbar
- [[31962]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31962) Add tooltip to 'configure' on datatable controls
- [[32027]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32027) Terminology: change "librarian interface" to "staff interface" in additional contents tool
- [[32194]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32194) "Can be guarantee" value should show uppercase "No"

  >This fixes the display of the patron categories "Can be guarantee" column so that "No" values have a capital "N". Previously, "no" values were shown with a lowercase "n".
- [[32236]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32236) Batch item modification - alignment of tick box for 'Use default values'
- [[32239]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32239) Report options for adding groups/sub groups are misaligned
- [[32257]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32257) Label for patron attributes misaligned on patron batch mod
- [[32261]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32261) Insufficient user feedback when selecting patron in autocomplete
- [[32272]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32272) Last borrower and previous borrower display on moredetail.pl is broken
- [[32355]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32355) Add class url to all URL syspref
- [[32368]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32368) Add page-section to saved report results
- [[32475]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32475) The phrase "System prefs" should be replaced with the correct terminology "System preferences"
- [[32504]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32504) Empty column name, misaligning visibility, and export for basket/orders in table settings
- [[32520]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32520) Patron autocomplete should respect DefaultPatronSearchFields
- [[32523]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32523) Shortcuts / Links to missing fields in MARC-Editor don't work as expected

  >This fixes the standard MARC editor so that the links for any errors go to the correct tab. Currently, the links only work if you are the correct tab.
- [[32568]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32568) Add page section to list of checkins

  >This patch adds the page-section class to the checkedin table on the returns page.
- [[32576]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32576) ILL needs the page-section treatment
- [[32596]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32596) Background jobs viewer not showing finished jobs
- [[32644]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32644) Terminology: staff/intranet and biblio in plugins home page

  >This patch replaces some incorrect terminology in the plugins home page regarding enhanced content plugins.
- [[32718]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32718) Capitalization: Display Order
- [[32733]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32733) Add more page-sections to basket summary page
- [[32768]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32768) Autocomplete suggestions container should always be on top of other UI elements
- [[32797]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32797) Cannot save OAI set mapping rule for subfield 0
- [[32881]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32881) System preferences sub menu text is hard to read
- [[32908]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32908) Item type icons broken in the bibliographic record details page
- [[32909]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32909) Item type icons broken when placing an item-level hold
- [[32941]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32941) Sys prefs side menu styling applying where not intended
- [[32982]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32982) 'Add/Edit group' modals in library groups is missing it's primary button
- [[33032]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33032) Alternate holdings broken in staff interface search results

### System Administration

- [[30694]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30694) Impossible to delete line in circulation and fine rules

  **Sponsored by** *Koha-Suomi Oy*
- [[32291]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32291) "library category" messages should be removed (not used)
- [[32535]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32535) BorrowerUnwantedField syspref should not include borrowers.flags
- [[32544]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32544) borrowers.flags should not be an option in any BorrowerMandatory or BorrowerUnwanted system preferences
- [[32761]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32761) Typos in description of CircControlReturnsBranch system preference
- [[32786]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32786) Curbside pickup admin page has cities search bar
- [[32787]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32787) Patron restrictions admin page has patron categories search bar
- [[32788]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32788) Curbside pickups - Order curbside pickup slots chronologically
- [[32803]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32803) EnableItemGroups and EnableItemGroupHolds options are wrong
- [[33004]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33004) Add VENDOR_TYPE to default authorised value categories

### Templates

- [[28235]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28235) Custom cover images are very large in staff search results and OPAC details
- [[31413]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31413) Set focus for cursor to Selector when adding a new audio alert
- [[31932]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=31932) The basket summary page template needs a cleanup
- [[32023]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32023) Remove horizontal line from OPAC navigation for CMS pages
- [[32061]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32061) <span> in the title of z39.50 servers page
- [[32074]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32074) Edit vendor has a floating toolbar, but still an additional save button at the bottom
- [[32159]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32159) Uncertain prices has 2 level 1 headings
- [[32200]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32200) Add page-section checkout notes page (circ)
- [[32205]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32205) Unnecessary sysprefs used in template params for masthead during failed OPAC auth
- [[32213]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32213) Reindent item search fields template
- [[32215]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32215) 'You Searched for' for patron restrictions is not used
- [[32222]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32222) Capitalization: id
- [[32226]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32226) Capitalization: Edit html content
- [[32229]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32229) Typo: Items missing from bundle at checkin for %s
- [[32230]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32230) Capitalization: Manage Domains
- [[32264]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32264) Capitalization/Terminology: Show in Staff client?
- [[32282]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32282) Capitalization: User id
- [[32283]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32283) Capitalization: opac users of this domain to login with this identity provider
- [[32289]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32289) Punctuation: Delete desk "...?"
- [[32290]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32290) ILL requests uses some wrong terminology
- [[32293]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32293) Terminology: Some budgets are not defined in item records
- [[32294]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32294) Capitalization: Enter your User ID...
- [[32295]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32295) Punctuation: Filters :
- [[32300]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32300) Add page-section to cataloguing plugins (cat)
- [[32307]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32307) Chocolat image viewer broken in the staff interface when Coce is enabled
- [[32320]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32320) Remove text-shadow from header menu links
- [[32323]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32323) Correct focus state of some DataTables controls
- [[32348]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32348) Library public is missing from columns settings
- [[32378]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32378) Incorrect label for in identity provider domains
- [[32400]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32400) Add page-section to tables for end of year rollover (acq)
- [[32482]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32482) Reindent holds awaiting pickup template

  >This tidies up the template used to display the holds awaiting pickup page (Circulation > Holds > Holds awaiting pickup). It also fixes the page so that the circulation sidebar is now shown.
- [[32562]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32562) Reindent the about page template
- [[32586]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32586) Reindent items with no checkouts reports template
- [[32587]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32587) Add page-section to items with no checkouts report
- [[32605]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32605) Restore some form styling from before the redesign
- [[32606]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32606) Revert Flatpickr style changes made in Bug 31943
- [[32616]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32616) Add 'page-section' to various acquisitions pages
- [[32618]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32618) Add 'page-section' to various administration pages
- [[32628]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32628) Add 'page-section' to various serials pages
- [[32632]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32632) Add 'page-section' to some tools pages
- [[32633]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32633) Add 'page-section' to cataloging and authority pages
- [[32672]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32672) Incorrect CSS path to jquery-ui
- [[32690]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32690) Reindent the serial collection template
- [[32738]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32738) Correct upload local cover image title tag
- [[32743]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32743) Reindent the invoice details page
- [[32757]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32757) "Save changes" button on housebound tab should be yellow
- [[32785]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32785) Typo: Maximum number of simultaneus pickups per interval (curbside pickups)
- [[32912]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32912) Use template wrapper for notices tabs
- [[32926]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32926) Cannot expand or collapse some System preference sections after a search
- [[32933]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32933) Use val() instead of attr("value") when getting field values with jQuery
- [[32973]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32973) Use template wrapper for breadcrumbs: about, main, and error page
- [[33011]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33011) Capitalization: Show in Staff interface?
- [[33015]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33015) 'Cancel' link still points to tools home when it should be cataloguing home on some pages
- [[33016]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33016) MARC diff view still shows tools instead of cataloging in title and breadcrumbs
- [[33048]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33048) Empty email link on error page when opac login not allowed
- [[33095]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33095) Text is white on white when hovering over pay/writeoff buttons in paycollect

### Test Suite

- [[28670]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28670) api/v1/patrons_holds.t is failing randomly
- [[29274]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=29274) z_reset.t is wrong
- [[32349]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32349) Remove TEST_QA
- [[32350]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32350) We should die if TestBuilder is passed a column we're not expecting
- [[32351]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32351) Fix all TestBuilder calls failing due to wrong column names
- [[32352]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32352) xt/check_makefile.t failing on node_modules
- [[32353]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32353) reserves.item_group_id should be undefined in tests by default
- [[32366]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32366) BatchDeleteBiblio task should have tests to prove indexing all takes place in one step
- [[32376]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32376) selenium/authentication_2fa.t produces artefact
- [[32622]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32622) Auth.t failing on D10
- [[32650]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32650) Koha/Holds.t is failing randomly
- [[32673]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32673) Remove misc/load_testing/ scripts
- [[32979]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32979) Add Test::Exception to Logger.t
- [[33054]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=33054) Koha/Acquisition/Order.t is failing randomly

### Tools

- [[22428]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22428) MARC modification template cuts text to 100 characters
- [[26628]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26628) Clubs permissions should grant access to Tools page
- [[30869]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=30869) Stock rotation rotas cannot be deleted

  **Sponsored by** *PTFS Europe*
- [[32255]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32255) Cannot use file upload in batch record modification
- [[32389]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32389) Syndetics links are built wrong on the staff results page
- [[32456]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32456) Date accessioned is now cleared when items are replaced
- [[32600]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32600) Housebound module needs page-section treatment
- [[32685]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32685) Display incorrect when matching authority records during import
- [[32967]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=32967) Recalls notices are using the wrong database columns

  **Sponsored by** *Catalyst*

## New system preferences
- CatalogConcerns
- CatalogerEmails
- OPACShowSavings
- OpacCatalogConcerns



## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha
documentation is

- [Koha Documentation](http://koha-community.org/documentation/)



The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)


## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (72.8%)
- Armenian (100%)
- Bulgarian (92.1%)
- Chinese (Taiwan) (83.1%)
- Czech (59.6%)
- English (New Zealand) (69%)
- English (USA)
- Finnish (95.5%)
- French (99.5%)
- French (Canada) (96.1%)
- German (100%)
- German (Switzerland) (50.6%)
- Greek (50.4%)
- Hindi (100%)
- Italian (93.7%)
- Nederlands-Nederland (Dutch-The Netherlands) (77.7%)
- Norwegian Bokmål (54%)
- Persian (59.1%)
- Polish (92.7%)
- Portuguese (74.1%)
- Portuguese (Brazil) (100%)
- Russian (92.9%)
- Slovak (59.7%)
- Spanish (99%)
- Swedish (76.3%)
- Telugu (78.8%)
- Turkish (89.1%)
- Ukrainian (78.3%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 22.06.00 is


- Release Manager: Tomás Cohen Arazi

- Release Manager assistants:
  - Jonathan Druart
  - Martin Renvoize

- QA Manager: Katrin Fischer

- QA Team:
  - Aleisha Amohia
  - Nick Clemens
  - David Cook
  - Jonathan Druart
  - Lucas Gass
  - Victor Grousset
  - Kyle M Hall
  - Martin Renvoize
  - Marcel de Rooy
  - Fridolin Somers

- Topic Experts:
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - REST API -- Martin Renvoize

- Bug Wranglers:
  - Aleisha Amohia
  - Indranil Das Gupta

- Packaging Manager: Mason James


- Documentation Manager: Caroline Cyr La Rose


- Documentation Team:
  - Aude Charillon
  - David Nind
  - Lucy Vaux-Harvey

- Translation Manager: Bernardo González Kriegel


- Wiki curators: 
  - Thomas Dukleth
  - Katrin Fischer

- Release Maintainers:
  - 22.11 -- PTFS Europe (Martin Renvoize, Matt Blenkinsop, Jacob O'Mara, Pedro Amorim)
  - 22.05 -- Lucas Gass
  - 21.11 -- Arthur Suzuki
  - 21.05 -- Wainui Witika-Park

## Credits
We thank the following libraries, companies, and other institutions who are known to have sponsored
new features in Koha 22.06.00

- [Association KohaLa](https://koha-fr.org)
- Bibliotheksservice-Zentrum Baden-Württemberg (BSZ)
- [Catalyst](https://www.catalyst.net.nz/products/library-management-koha)
- Education Services Australia SCIS
- Horowhenua Libraries Trust
- [Koha-Suomi Oy](https://koha-suomi.fi)
- [PTFS Europe](https://ptfs-europe.com)
- [The New Zealand Institute for Plant and Food Research Limited](https://www.plantandfood.com/en-nz/)
- The Research University in the Helmholtz Association (KIT)
- Virginia Polytechnic Institute and State University

We thank the following individuals who contributed patches to Koha 22.06.00

- Aleisha Amohia (10)
- Pedro Amorim (18)
- Tomás Cohen Arazi (74)
- Alex Arnaud (2)
- Andrew Auld (3)
- Matt Blenkinsop (13)
- Jérémy Breuillard (1)
- Alex Buckley (1)
- Nick Clemens (52)
- David Cook (18)
- Frédéric Demians (4)
- Jonathan Druart (141)
- Magnus Enger (1)
- Katrin Fischer (80)
- Géraud Frappier (1)
- Andrew Fuerste-Henry (2)
- Lucas Gass (22)
- Didier Gautheron (1)
- Victor Grousset (1)
- Thibaud Guillot (2)
- Michael Hafen (3)
- Kyle M Hall (36)
- Mason James (4)
- Jan Kissig (1)
- Owen Leonard (65)
- The Minh Luong (1)
- Julian Maurice (18)
- Matthias Meusburger (3)
- Agustín Moyano (4)
- David Nind (5)
- Jacob O'Mara (4)
- Mona Panchaud (1)
- Johanna Raisa (1)
- Martin Renvoize (71)
- Marcel de Rooy (23)
- Caroline Cyr La Rose (6)
- Andreas Roussos (3)
- Slava Shishkin (2)
- Fridolin Somers (8)
- Emmi Takkinen (4)
- George Veranis (1)
- Shi Yao Wang (2)
- Jenny Way (1)
- Hammat Wele (3)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 22.06.00

- Athens County Public Libraries (65)
- BibLibre (35)
- Bibliotheksservice-Zentrum Baden-Württemberg (BSZ) (80)
- ByWater-Solutions (110)
- Catalyst (1)
- Catalyst Open Source Academy (10)
- Dataly Tech (4)
- David Nind (5)
- dubcolib.org (2)
- Independant Individuals (7)
- Koha Community Developers (142)
- Koha-Suomi (4)
- KohaAloha (4)
- Libriotech (1)
- mpan.ch (1)
- Prosentient Systems (18)
- PTFS-Europe (109)
- Rijksmuseum (23)
- Solutions inLibro inc (13)
- Tamil (4)
- th-wildau.de (1)
- Theke Solutions (78)

We also especially thank the following individuals who tested patches
for Koha

- Michael Adamyk (1)
- Pedro Amorim (30)
- Tomás Cohen Arazi (638)
- Axelle (2)
- Matt Blenkinsop (35)
- Philippe Blouin (1)
- Christopher Brannon (1)
- Felicity Brown (2)
- Alex Buckley (1)
- Nick Clemens (103)
- Bob Bennhoff - CLiC (7)
- David Cook (7)
- Chris Cormack (2)
- David (2)
- Frédéric Demians (6)
- Paul Derscheid (7)
- Jonathan Druart (89)
- emlam (1)
- Laura Escamilla (4)
- Jonathan Field (1)
- Katrin Fischer (142)
- Andrew Fuerste-Henry (25)
- Lucas Gass (56)
- Amaury GAU (2)
- Victor Grousset (6)
- Amit Gupta (1)
- Kyle M Hall (97)
- Evelyn Hartline (1)
- Sally Healey (3)
- Heather Hernandez (7)
- Mason James (1)
- Barbara Johnson (5)
- Janusz Kaczmarek (1)
- Jan Kissig (2)
- Emily Lamancusa (1)
- Owen Leonard (44)
- LMSCloudPaulD (1)
- Marius Mandrescu (1)
- Marie-Luce (2)
- Johanna Miettunen (1)
- ml-inlibro (1)
- Agustín Moyano (14)
- Solene Ngamga (2)
- David Nind (173)
- Jacob O'Mara (1)
- Helen Oliver (22)
- Jacob Omara (1)
- Pascal (1)
- Martin Renvoize (147)
- Phil Ringnalda (1)
- Marcel de Rooy (55)
- Caroline Cyr La Rose (16)
- Danyon Sewell (1)
- Michaela Sieber (10)
- Fridolin Somers (12)
- Emmi Takkinen (1)
- George Veranis (1)
- Hammat Wele (3)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to koha-devel@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](https://git.koha-community.org/koha-community/koha)

The branch for this version of Koha and future bugfixes in this release
line is master.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 10 Mar 2023 14:56:54.
