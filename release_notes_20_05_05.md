# RELEASE NOTES FOR KOHA 20.05.04
08 Oct 2020

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.05.04 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.05.04.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.05.04 is a bugfix/maintenance release.

It includes 4 new features, 237 enhancements, 259 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required



## New features

### Architecture, internals, and plumbing

- [[22343]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22343) Add configuration options for SMTP servers

  >This patchset adds the ability to set SMTP servers and then pick them for using on each library when sending notices, cart, etc.
  >
  >SSL/TLS authentication is supported.
  >
  >A new administration page is added for managing the servers.
- [[22417]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22417) Add a task queue
- [[26290]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26290) Add the ability to set a default SMTP server in koha-conf.xml

  >With this enhancement, systems administrators can set a default/global SMTP configuration when creating the Koha instance, or by manually editing the koha-conf.xml.

### Circulation

- [[21946]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21946) Group circulation by item type

## Enhancements

### Acquisitions

- [[23682]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23682) Add ability to manually import EDI invoices as an alternative to automatic importing on download
- [[24157]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24157) Additional acquisitions permissions

  **Sponsored by** *Galway-Mayo Institute of Technology*
- [[25033]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25033) Counts of suggestions are confusing

  >This patch adds to the simple count of all suggestions in the system with a count filtered by the users branch. This means that on the homepage and other areas a user will see a count of local suggestions and total suggestions in the system.
  >
  >Previously clicking the link to suggestions would take the user to a page showing fewer suggestions that counted in the link. Now these numbers hsould be more consistent.
- [[26014]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26014) Add publication year and edition to Z39.50 results in acquisition
- [[26089]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26089) Add acquisitions-related reports to acquisitions sidebar menu

### Architecture, internals, and plumbing

- [[21395]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21395) Make perlcritic happy
- [[22393]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22393) Remove last remaining manualinvoice use
- [[23070]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23070) Use Koha::Hold in C4::Reserves::RevertWaitingStatus
- [[23092]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23092) Add 'daterequested' to the transfers table
- [[23166]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23166) Simplify code related to orders in catalogue/*detail.pl
- [[23632]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23632) Remove C4::Logs::GetLogs
- [[24306]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24306) Add debug option to koha-indexer
- [[25070]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25070) Include files to display address and contact must be refactored

  >This internal change simplifies the code for editing and displaying patron address and contact information. It removes duplicated code, reducing potential problems when the code is changed in the future.
  >
  >For example, there are currently 5 include files for each value of the address format (us, de, fr) with the code duplicated for each language. The change reduces the need for 5*3 files to 5 files.
- [[25114]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25114) Remove duplicated logic from GetLoanLength()
- [[25287]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25287) Add columns_settings support to API datatables wrapper
- [[25511]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25511) Add --force option to update_dbix_class_files.pl
- [[25663]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25663) Koha::RefundLostItemFeeRules should be merged into Koha::CirculationRules
- [[25723]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25723) Improve efficiency of holiday calculation
- [[25998]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25998) Add 'library' relation to Koha::Account::Line
- [[26132]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26132) Improve readability of TooMany
- [[26133]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26133) Unneeded calls in detail.pl can be removed
- [[26141]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26141) Duplicated code in search.pl
- [[26251]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26251) Remove unused routines from svc/split_callnumbers
- [[26268]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26268) Remove items.paid for once and for all
- [[26325]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26325) Add primary_key_exists function to Installer.pm
- [[26394]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26394) .mailmap needs to be updated
- [[26425]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26425) Fix history.txt once and for all
- [[26432]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26432) Remove unused ModZebrations
- [[26524]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26524) Add Koha::Acquisition::Basket->orders

### Cataloging

- [[5428]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=5428) Back to results after deleting a record
- [[15851]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15851) Only display "Analytics: Show analytics" when records have linked analytics

  **Sponsored by** *Orex Digital*
- [[16314]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16314) Show upload link for upload plugin in basic MARC editor
- [[19322]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19322) Typo in UNIMARC field 140 plugin
- [[19327]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19327) Typo in UNIMARC field 128a plugin
- [[20154]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20154) Stay in the open tab when editing authority record
- [[24134]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24134) Add placeholder for 2 digit years to allow autogeneration of dates in 008
- [[25728]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25728) Add the ability to create a new authorised value within the cataloguing module

  **Sponsored by** *Orex Digital*
- [[26330]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26330) jQueryUI tabs don't work with non-Latin-1 characters

### Circulation

- [[16112]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16112) Specify renewal date for batch renew
- [[16748]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16748) Batch checkout needs set due date
- [[20469]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20469) Add item status to staff article requests form
- [[21750]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21750) Move collection to its own column in checkins table
- [[24159]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24159) Allow daysMode for calculating due and renewal dates to be set at the circulation rules level

  **Sponsored by** *Institute of Technology Carlow*
- [[24176]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24176) Make datelastborrowed a column in holdings_table
- [[24201]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24201) Attach desk to intranet session
- [[25232]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25232) Add ability to skip trapping items with a given notforloan value
- [[25261]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25261) Multiple parts handling - confirmation alert

  **Sponsored by** *PTFS Europe* and *Royal College of Music*
- [[25430]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25430) Improve the styling of the claims returned tab
- [[25534]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25534) Add ability to send an email specifying a reason and store the reason when canceling a hold

  >This feature allows to give a reason on cancelling a hold and to send an email to the patron with the information. In order to achieve this it adds a new column cancellation_reason to the reserves and old_reserves tables, used to store the value form the new authorised value category CANCELLATION_REASON. The new notice uses the code HOLD_CANCELLATION.
- [[25699]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25699) Add edition information to Holds to pull report
- [[25717]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25717) Improve messages for automatic renewal errors

  >This change improves the wording and grammar for automatic renewal error messages.
- [[25798]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25798) Copyright year to Holds to pull report
- [[25799]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25799) Edition information to Holds queue report
- [[25907]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25907) When cancelling a waiting hold on returns.pl, looks for new hold to fill without rescanning barcode
- [[26501]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26501) Article requests: Add datatables to requests form in staff client

### Command-line Utilities

- [[21591]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21591) Data inconsistencies - Item types and biblio level
- [[23696]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23696) build_oai_sets.pl should take biblios from deletedbiblio_metadata too
- [[24152]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24152) Add the ability to purge pseudonymized data

  **Sponsored by** *Association KohaLa*
- [[24153]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24153) Add a confirm flag to the cleanup_database.pl cronjob

  **Sponsored by** *Association KohaLa*
- [[26175]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26175) Remove warn if undefined barcode in misc/export_records.pl

### Course reserves

- [[14648]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14648) Batch remove reserve items
- [[25606]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25606) Adds "Remove all reserves" button to course details

### Fines and fees

- [[8338]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=8338) Add ability to remove fines with dropbox mode
- [[19036]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19036) Number payment receipts / payment slips
- [[24610]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24610) Let user switch between 'Pay' and 'Write off' mode
- [[26160]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26160) Add column configuration to sales table to allow hiding of 'Code' column
- [[26161]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26161) Confirm and cancel buttons should be underneath the right hand form on the POS page

### Hold requests

- [[19889]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19889) LocalHoldsPriority needs exclusions

  **Sponsored by** *Cooperative Information Network (CIN)*
- [[22789]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22789) Establish non-priority holds
- [[23820]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23820) Club hold pickup locations should be able to default to patron's home library

  **Sponsored by** *South East Kansas Library System*
- [[25892]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25892) Clarify the visual hierarchy of holds by library and itemtype
- [[26281]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26281) Add cancellation reason to holds history

### I18N/L10N

- [[25443]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25443) Improve translation of "Select the host record to link%s to"
- [[25922]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25922) aria-labels are currently not translatable
- [[26065]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26065) Move translatable strings out of marc_modification_templates.tt and into marc_modification_templates.js
- [[26118]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26118) Move translatable strings out of tags/review.tt and into tags-review.js
- [[26217]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26217) Move translatable strings out of templates into acq.js
- [[26225]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26225) Move translatable strings out of audio_alerts.tt and into audio_alerts.js
- [[26229]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26229) Move translatable strings out of categories.tt and into categories.js
- [[26230]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26230) Move translatable strings out of item_search_fields.tt and into item_search_fields.js
- [[26237]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26237) Move translatable strings out of preferences.tt and into JavaScript files
- [[26240]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26240) Move translatable strings out of sms_providers.tt and into sms_providers.js
- [[26242]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26242) Move translatable strings out of results.tt and into results.js
- [[26243]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26243) Move translatable strings out of templates and into circulation.js
- [[26256]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26256) Move translatable strings out of templates and into serials-toolbar.js

### ILL

- [[20799]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20799) Add a link from biblio detail view to ILL request detail view, if a biblio has an ILL request
- [[23391]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23391) Hide finished ILL requests

### Installation and upgrade (web-based installer)

- [[24973]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24973) Allow to localize and translate system preferences with new yaml based installer
- [[25129]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25129) Update German (de-DE) web installer files for 20.05

### Lists

- [[24884]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24884) Remove 'New list' button in 'Public lists' tab if OpacAllowPublicListCreation is disabled

### MARC Bibliographic data support

- [[24322]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24322) National Library of Medicine (NLM) call number to XSLT Detail

### MARC Bibliographic record staging/import

- [[15437]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15437) MARC21: Show $i for 780/785

### Notices

- [[16371]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16371) Quote of the Day (QOTD) for the staff interface

  **Sponsored by** *Koha-Suomi Oy*

  >This enhancement lets you choose where the Quote of the Day (QOTD) is displayed:
  >- OPAC: QOTD only appears in the OPAC.
  >- Staff interface: QOTD only appears in the staff interface.
  >- Both [Select all]: QOTD appears in the staff interface and OPAC.
- [[24197]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24197) Custom destination for failed overdue notices

  **Sponsored by** *Catalyst*
- [[24591]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24591) Add developer script to preview a letter
- [[25097]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25097) Add option to message_queue to allow for only specific sending notices
- [[25639]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25639) Add search queries to HTML so queries can be retrieved via JS

  >This patch adds global JS variables for the prepared search forms: query_desc_query_cgi, and query
  >
  >These are useful for plugins or custom JS wishing to perform searches outside of Koha and incorporate results.
- [[25776]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25776) Add last updated date for notices and slips

### OPAC

- [[8732]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=8732) Add a system preference to allow users to choose to display an icon based on the Koha bibliographic level itemtype
- [[18911]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18911) Option to set preferred language in OPAC
- [[20168]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20168) Update of the OPAC bootstrap template to bootstrap v4
- [[21066]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21066) Replace opac_news.timestamp by published_on and add updated_on as timestamp
- [[22807]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22807) Accessibility: Add 'Skip to main content' link
- [[23795]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23795) Convert OpacCredits system preference to news block
- [[23796]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23796) Convert OpacCustomSearch system preference to news block
- [[23797]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23797) Convert OpacLoginInstructions system preference to news block
- [[24405]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24405) Links in facets are styled differently than other links on the results page in OPAC
- [[25151]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25151) Accessibility: The 'Your cart' page does not contain a level-one header
- [[25154]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25154) Accessibility: The 'Search results' page does not use heading markup where content is introduced
- [[25155]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25155) Accessibility: The 'Login modal' contains semantically incorrect headings
- [[25236]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25236) Accessibility: The 'Refine your search' box contains semantically incorrect headings
- [[25237]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25237) Accessibility: The 'Author details' in the full record display contains semantically incorrect headings
- [[25238]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25238) Accessibility: Multiple 'H1' headings exist in the full record display
- [[25239]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25239) Accessibility: The 'Confirm hold page' contains semantically incorrect headings
- [[25242]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25242) Accessibility: The 'Holdings' table partially obscures navigation links at 200% zoom
- [[25244]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25244) Accessibility: Checkboxes on the search results page do not contain specific aria labels
- [[25402]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25402) Put OPAC cart download options into dropdown menu

  >This enhancement adds the OPAC cart download format options into the dropdown menu, rather than opening in a separate pop up window. (This also matches the behaviour in the staff interface.)
- [[25771]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25771) Allow the user to sort checkouts by the renew column in the OPAC
- [[25801]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25801) Add itemnumber parameter to opac-detail
- [[25871]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25871) Add "only library" to OpacItemLocation options
- [[25984]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25984) Accessibility: Shelf browse lacks focus visibility when cover image is missing
- [[26008]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26008) Remove the use of jquery.checkboxes plugin from OPAC cart
- [[26039]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26039) Accessibility: Shelf browser is not announced upon loading
- [[26041]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26041) Accessibility: The date picker calendar is not keyboard accessible
- [[26094]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26094) "Suggest for Purchase" button missing unique CSS class
- [[26266]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26266) Add jQuery validator to opac-password-recovery.tt
- [[26299]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26299) Help text for OPAC SMS number should be less North American-centric
- [[26454]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26454) Add system preference to set meta description for the OPAC

  >Functionality to add meta description tag with content with the system preference OpacMetaDescription. This is used by search engines to add a description to the library in search results.

### Patrons

- [[6725]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=6725) Make patron duplicate matching flexible
- [[10910]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=10910) Add a warn when deleting a patron with pending suggestions
- [[20057]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20057) Auto-approve option for borrower modifications
- [[21345]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21345) Patron records with attached files not obvious from patron details view
- [[22087]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22087) Show city and state in patron search results
- [[23816]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23816) Allow to have different password strength and length settings for different patron categories

  **Sponsored by** *Northeast Kansas Library - NEKLS*
- [[24151]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24151) Add a pseudonymization process for patrons and transactions

  **Sponsored by** *Association KohaLa*
- [[25364]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25364) Add "Other" to the gender options in a patron record
- [[25654]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25654) Make the contact and non-patron guarantor sections separate on patron entry form

### Plugin architecture

- [[21468]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21468) Plugins need hooks for checkin and checkout actions
- [[24031]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24031) Add plugin hook after_hold_create
- [[25855]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25855) Add hook to AddRenewal using a new _after_circ_actions method in circulation

  **Sponsored by** *ByWater Solutions*
- [[25961]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25961) Add hooks for plugins to inject variables to XSLT

  >This enhancement adds the following plugin hooks:
  >- opac_results_xslt_variables
  >- opac_detail_xslt_variables
  >
  >This allows us to pass valuable information for XSLT customization.
  >
  >Plugins implementing this hooks, should return a hashref containing the variable names and values to be passed to the XSLT processing code.
- [[26063]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26063) Use Koha::Plugins->call for other hooks

  **Sponsored by** *ByWater Solutions*
- [[26338]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26338) Show tool plugins run in tools home

### Reports

- [[24958]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24958) Remember last selected tab in SQL reports
- [[25605]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25605) Exporting report as a tab delimited file can produce a lot of warnings
- [[26269]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26269) "Download file of displayed overdues" in Circulation->Overdues produces .csv with fewer results than the displayed list under certain circumstances

### SIP2

- [[21979]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21979) Add option to SIP2 config to send arbitrary item field in CR instead of collection code
- [[24165]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24165) Add ability to send any item field in a library chosen SIP field
- [[25344]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25344) Add support for circulation status 10 ( item in transit )
- [[25347]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25347) Add support for circulation status 11 ( claimed returned )
- [[25348]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25348) Add support for circulation status 12 ( lost )
- [[25541]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25541) Add ability to prevent checkin via SIP of items with holds

### Searching

- [[25867]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25867) Label holdingbranch as Current library rather than Current location

### Searching - Elasticsearch

- [[24155]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24155) Weights should be (optionally) applied to Advanced search

  >This patch adds the weighting of search results to searches made via the 'Advanced search' interface.
  >
  >Weights, defined in Administration section, boost ranking of results when specified fields are matched in a search query.
  >
  >The weights will not affect index-specific queries, but are useful for keyword or queries with limits applied and so should be applied unless the user specifies not to.

### Serials

- [[26484]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26484) Add serials-related reports to serials sidebar menu

### Staff Client

- [[12093]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=12093) Add CSS classes to item statuses in detail view
- [[15400]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15400) Display patron age in useful places in the staff interface

  **Sponsored by** *Catalyst*
- [[26007]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26007) Warning/reminder for changes to Koha to MARC mapping

  >In current versions of Koha you can no longer change the Koha to MARC mappings from the frameworks, but only from the Koha to MARC mapping page in administration. This patch cleans up the hints on the framework page and adds a well visible note on the Koha to MARC mapping page. Any changes to the mappings require that you run the batchRebuildBiblioTables script to fully take effect.
- [[26182]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26182) Clearly pair UpdateItemWhenLostFromHoldList and CanMarkHoldsToPullAsLost system preferences
- [[26458]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26458) Get item details using only itemnumber
- [[26473]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26473) Get items for editing using only itemnumber

### System Administration

- [[20815]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20815) Add ability to choose if lost fee is refunded based on length of time item has been lost
- [[22844]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22844) Simplify the process of selecting database columns for system preferences

  >This enhancement introduces a new way to select database columns for selected system preferences like BorrowerMandatoryField. Currently, this requires manually adding the database field names. The enhancement lets you select from a list of available fields in a new window, and also select and clear all fields.
  >
  >This is implemented for these system preferences:
  >- BorrowerMandatoryField
  >- BorrowerUnwantedField
  >- PatronQuickAddFields
  >- PatronSelfModificationBorrowerUnwantedField
  >- PatronSelfRegistrationBorrowerMandatoryField
  >- PatronSelfRegistrationBorrowerUnwantedField
  >- StatisticsFields
  >- UniqueItemFields
- [[25288]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25288) Make the libraries list use the API
- [[25630]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25630) More capitalization and terminology fixes for system preferences

  >This enhancement makes changes to the descriptions for many of the system preferences to help improve consistency with the terminology list* and readability.
  >
  >The changes made cover:
  >- Capitalization (such as Don't Allow to Don't allow).
  >- Terminology (such as staff client to staff interface, including the tab label).
  >- Punctuation (such as the placement of periods/full stops at the end of sentences).
  >- Readability (rearranging or rephrasing the description to make easier to understand).
  >
  >Some of the terminology changes include:
  >- bib and biblio => bibliographic
  >- branch => library
  >- borrower => patron
  >- Do not > Don't
  >- staff client => staff interface
  >- pref and syspref => system preference
  >
  >* https://wiki.koha-community.org/wiki/Terminology
- [[25709]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25709) Rename systempreference from NotesBlacklist to NotesToHide

  >This patchset updates a syspref name to be clearer about what it does and to follow community guidelines on using inclusive language.
  >
  >https://wiki.koha-community.org/wiki/Coding_Guidelines#TERM3:_Inclusive_Language
- [[25945]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25945) Description of AuthoritySeparator is misleading

### Templates

- [[23148]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23148) Replace Bridge icons with transparent PNG files
- [[23410]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23410) Add submenus to system preferences sidebar menu
- [[24156]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24156) Basket - Make sort order and number of items to display configurable

  **Sponsored by** *Institute of Technology Tallaght*

  >This patch adds new options in the Table settings section to make the sort order and number of results per page in the basket table configurable.
- [[24625]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24625) Phase out jquery.cookie.js:  showLastPatron
- [[25031]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25031) Improve handling of multiple covers on the biblio detail page in the staff client

  >This enhancement improves the display of multiple covers for a record in the staff interface, including covers from these services:
  >- Amazon
  >- Local cover images (including multiple local cover images)
  >- Coce (serving up Amazon, Google, and OpenLibrary images)
  >- Images from the CustomCoverImages preference
- [[25317]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25317) Move translatable strings out of additem.js.inc
- [[25320]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25320) Move translatable strings out of merge-record-strings.inc into merge-record.js
- [[25321]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25321) Move translatable strings out of strings.inc into the corresponding JavaScript
- [[25351]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25351) Move cart-related strings out of opac-bottom.inc and into basket.js
- [[25363]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25363) Merge common.js with staff-global.js
- [[25427]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25427) Make authority subfield management interface consistent with bibliographic subfield management view
- [[25471]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25471) Add DataTables to MARC subfield structure admin page for bibliographic frameworks
- [[25593]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25593) Terminology: Fix "There is no order for this biblio." on catalog detail page
- [[25627]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25627) Move OPAC problem reports from administration to tools
- [[25687]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25687) Switch Y/N in EDI accounts table for Yes and No for better translatability
- [[25727]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25727) Update the Select2 JS lib

  **Sponsored by** *Orex Digital*
- [[25827]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25827) Add floating toolbar to the holds summary page in staff interface
- [[25832]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25832) Add DataTables to MARC subfield structure admin page for authorities
- [[25879]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25879) Improve display of guarantor information in the patron entry form
- [[25906]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25906) Style corrections for OPAC serial pages
- [[25968]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25968) Make logs sort by date descending as a default and add column configuration options
- [[26004]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26004) Remove unused jQuery plugin jquery.hoverIntent.minified.js from the OPAC
- [[26010]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26010) Remove the use of jquery.checkboxes plugin from staff interface cart
- [[26011]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26011) Remove unused jQuery plugin jquery.metadata.min.js from the OPAC
- [[26015]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26015) Terminology: staff interface should be used everywhere
- [[26016]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26016) Capitalization: MARC Preview
- [[26060]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26060) Replace staff interface table sort icons with SVG
- [[26061]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26061) Improve style of sidebar datepickers
- [[26085]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26085) Add the copy, print and export DataTables buttons to lost items report
- [[26087]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26087) Add table configuration and export options to orders by fund report
- [[26091]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26091) Add column configuration and export options to catalog statistics report
- [[26120]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26120) Remove the use of jquery.checkboxes plugin from tags review template
- [[26149]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26149) Remove jquery.checkboxes plugin from problem reports page
- [[26150]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26150) Remove the use of jquery.checkboxes plugin from inventory page
- [[26151]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26151) Remove the use of jquery.checkboxes plugin from suggestions management page
- [[26153]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26153) Remove the use of jquery.checkboxes plugin from items lost report
- [[26159]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26159) Remove the use of jquery.checkboxes plugin from batch record delete page
- [[26164]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26164) Replace OPAC table sort icons with SVG
- [[26194]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26194) Messages about missing cash registers should link to cash register management
- [[26201]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26201) Remove the use of jquery.checkboxes plugin from batch extend due dates page
- [[26202]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26202) Remove the use of jquery.checkboxes plugin from batch record modification page
- [[26204]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26204) Remove the use of jquery.checkboxes plugin from staff interface lists
- [[26212]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26212) Remove the use of jquery.checkboxes plugin from pending offline circulations
- [[26214]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26214) Remove the use of jquery.checkboxes plugin on late orders page
- [[26215]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26215) Remove the use of jquery.checkboxes plugin from Z39.50 search pages
- [[26216]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26216) Remove the use of jquery.checkboxes plugin from catalog search results
- [[26245]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26245) Remove unused functions from members.js
- [[26261]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26261) Split calendar.inc into include file and JavaScript file
- [[26291]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26291) Move translatable strings out of z3950_search.inc into z3950_search.js
- [[26334]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26334) Move translatable strings out of members-menu.inc into members-menu.js
- [[26339]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26339) Move translatable strings out of addorderiso2709.tt into addorderiso2709.js
- [[26395]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26395) Move translatable strings out of letter.tt into letter.js
- [[26419]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26419) Replace OPAC Koha logo with SVG
- [[26441]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26441) Move translatable strings out of catalog-strings.inc into catalog.js
- [[26504]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26504) Remove the use of jquery.checkboxes plugin from checkout notes page
- [[26530]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26530) Use patron card number as checkbox label during patron merge

### Test Suite

- [[25113]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25113) Make CirculationRules.t flexible for new scope combinations

  **Sponsored by** *National Library of Finland*
- [[26157]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26157) Redirect expected DBI warnings
- [[26462]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26462) t/db_dependent/Holds.t tests delete data unnecessarily

### Tools

- [[4985]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=4985) Copy a change on the calendar to all libraries

  **Sponsored by** *Koha-Suomi Oy*
- [[5087]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=5087) Option to not show CSV profiles in OPAC

  **Sponsored by** *Catalyst*

  >This patch adds an option to show or not show a CSV profile in the OPAC cart and lists download formats list.
- [[22660]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22660) Allow use of CodeMirror for editing HTML in the news editor
- [[23114]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23114) Inventory: allow to scan barcodes into input field
- [[25101]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25101) Add ability to skip previewing results when batch extending due dates
- [[25694]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25694) Add ability to delete a MARC modification template when viewing
- [[25845]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25845) Cannot limit system logs to 'api' interface
- [[26013]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26013) Date on 'manage staged MARC records' is not formatted correctly
- [[26086]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26086) Add a 'cron' interface limit to the log viewer
- [[26207]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26207) Compare values of system preference log entries
- [[26431]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26431) Use split button to offer choice of WYSIWYG or code editor for news


## Critical bugs fixed

### Acquisitions

- [[14543]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14543) Order lines updated that have a tax rate not in gist will have tax rate set to 0!
- [[25677]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25677) Checkbox options for EDI accounts cannot be enabled
- [[25750]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25750) Fallback to ecost_tax_included, ecost_tax_excluded not happening when no 'Actual cost' entered

  **Sponsored by** *Horowhenua District Council*
- [[26082]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26082) Follow up to bug 23463 - need to call Koha::Item store to get itemnumber
- [[26134]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26134) Error when adding to basket from new/staged file when using MARCItemFieldsToOrder
- [[26438]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26438) Follow up to bug 23463 - return from Koha::Item overwrites existing variable

### Architecture, internals, and plumbing

- [[23634]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23634) Privilege escalation vulnerability for staff users with 'edit_borrowers' permission and 'OpacResetPassword' enabled
- [[24663]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24663) OPACPublic must be tested for all opac scripts
- [[24986]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24986) Maximum row size reached soon for borrowers and deletedborrowers
- [[25504]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25504) Wrong API spec breaks plack without meaningful error
- [[25634]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25634) koha-foreach exits too early if any command has non-zero status
- [[25707]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25707) Mappings update in bug 11529 causes incorrect MARC to DB data flow
- [[25909]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25909) Recent change to datatables JS in the OPAC causes errors
- [[25964]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25964) Data loss possible when items are modified
- [[26253]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26253) duplicated mana_config in etc/koha-conf.xml
- [[26322]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26322) REST API plugin authorization is not checked anymore
- [[26341]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26341) Database update for bug 21443 is not idempotent and will destroy settings
- [[26434]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26434) Plugin dirs duplicates in @INC with plack
- [[26470]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26470) itemnumber not available for plugin hook

### Cataloging

- [[26083]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26083) Item editor defaults to lost

### Circulation

- [[18501]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18501) Automatic refunds need protection from failure
- [[25566]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25566) Change in DecreaseLoanHighHolds behaviour
- [[25726]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25726) Holds to Pull made empty by pathological holds
- [[25758]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25758) Items scheduled for automatic renewal do not show that they will not renew due to a hold
- [[25783]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25783) Holds Queue treating item-level holds as bib-level
- [[25850]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25850) CalcDateDue freezes with 'useDaysMode' set to 'Dayweek' and the due date lands on a Sunday
- [[25851]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25851) 19.11 upgrade creates holdallowed rule with empty value
- [[25969]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25969) Checking in a found hold at a different branch then confirming the hold causes internal server error
- [[26078]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26078) "Item returns to issuing library" creates infinite transfer loop
- [[26108]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26108) Checkins should not require item to have been checked out
- [[26510]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26510) Transport Cost Matrix editor doesn't show all data when HoldsQueueSkipClosed is enabled

### Command-line Utilities

- [[25538]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25538) koha-shell should pass --login to sudo if no command
- [[25683]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25683) update_patron_categories.pl should recognize no fine history = 0 outstanding fines
- [[25752]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25752) Current directory not kept when using koha-shell

### Database

- [[24379]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24379) Patron login attempts happen to be NULL instead of 0
- [[25826]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25826) Hiding biblionumber in the frameworks breaks links in result list

### Fines and fees

- [[25526]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25526) Using Write Off Selected will not allow for a different amount to be written off
- [[26023]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26023) Incorrect permissions handling for cashup actions on the library level registers summary page

### Hold requests

- [[18958]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18958) If patron has multiple record level holds on one record transferring first hold causes next hold to become item level
- [[24683]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24683) Holds on biblios with different item types: rules for holds allowed are not applied correctly if any item type is available
- [[25786]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25786) Holds Queue building may target the wrong item for item level requests that match holds queue priority

### I18N/L10N

- [[26158]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26158) Z3950 search button broken for translations

### ILL

- [[26114]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26114) ILL should mark status=RET only if a return happened

### Installation and upgrade (command-line installer)

- [[26265]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26265) Makefile.PL is missing pos directory

### Label/patron card printing

- [[25852]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25852) If a layout is edited, the layout type will revert to barcode

### MARC Authority data support

- [[25273]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25273) Elasticsearch Authority matching is returning too many results
- [[25653]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25653) Authorities search does not retain selection

### MARC Bibliographic record staging/import

- [[26231]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26231) bulkmarcimport.pl does not import authority if it already has a 001 field

### Notices

- [[26420]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26420) Overdue notices script does not care about borrower's language, always takes default template

### OPAC

- [[17842]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17842) Broken diacritics on records exported as MARC from cart
- [[22672]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22672) Replace <i> tags with <em> AND <b> tags with <strong> in the OPAC
- [[25492]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25492) Your Account Menu button does nothing on mobile devices
- [[25769]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25769) Patron self modification triggers change request for date of birth to null
- [[26005]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26005) OPAC cart display fails with error

  >This fixes a problem with the OPAC cart - it should now work correctly when opened, instead of generating an error message.
- [[26037]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26037) openlibrary.org is hit on every Koha requests
- [[26069]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26069) Twitter share button leaks information to Twitter
- [[26505]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26505) Suspend hold modal broken in the OPAC

### Packaging

- [[17084]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17084) Automatic debian/control updates (master)
- [[25591]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25591) Update list-deps for Debian 10 and Ubuntu 20.04
- [[25633]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25633) Update debian/control.ini file for 20.05 release cycle
- [[25693]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25693) Correct permissions must be set on logdir after an upgrade
- [[25792]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25792) Rename 'ttf-dejavu' package to 'fonts-dejavu' for Debian 11
- [[25828]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25828) Update cpanfile for 20.05 release cycle
- [[25920]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25920) Add liblocale-codes-perl package to fix ubuntu-stable (focal)

### Patrons

- [[25322]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25322) Adding a guarantor with no relationship defaults to first available relationship name
- [[25858]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25858) Borrower permissions are broken by update from bug 22868
- [[26285]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26285) Use country code + number (E.164) validation for SMS numbers
- [[26556]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26556) Cities autocomplete broken in patron edition

### Plugin architecture

- [[26138]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26138) Errors if enable_plugins is zero

### REST API

- [[23653]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23653) Plack fails when http://swagger.io/v2/schema.json is unavailable and schema cache missing
- [[24003]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24003) REST API should set C4::Context->userenv
- [[25774]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25774) REST API searches don't handle correctly utf8 characters
- [[25944]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25944) Bug in ill_requests patch schema
- [[26143]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26143) The API does not handle requesting all resources

### Reports

- [[26090]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26090) Catalog by itemtype report fails if SQL strict mode is on

### SIP2

- [[25992]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25992) SIP2 server doesn't start - Undefined subroutine set_logger

### Searching - Elasticsearch

- [[25265]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25265) Elasticsearch - Batch editing items on a biblio can lead to incorrect index
- [[25864]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25864) Case sensitivity breaks searching of some fields in ES5
- [[25882]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25882) Elasticsearch - Advanced search itemtype limits are being double quoted
- [[26309]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26309) Elasticsearch cxn_pool must be configurable (again)
- [[26507]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26507) New items not indexed

### Searching - Zebra

- [[23086]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23086) Search for collection is broken

### System Administration

- [[25651]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25651) Modifying an authorised value make it disappear

### Templates

- [[25839]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25839) Typo patron.streetype in member-main-address-style.inc
- [[25842]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25842) Typo "streetype" in member-main-address-style.inc

### Test Suite

- [[26031]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26031) www/search_utf8.t is failing randomly
- [[26033]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26033) framapic is closing
- [[26250]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26250) Test suite does not pass if Elastic is used as search engine

### Tools

- [[15032]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15032) [Plack] Scripts that fork (like stage-marc-import.pl) don't work as expected
- [[25557]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25557) Column config table in acquisitions order does not match the acq table in baskets

### Z39.50 / SRU / OpenSearch Servers

- [[23542]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23542) SRU import encoding issue


## Other bugs fixed

### About

- [[7143]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=7143) Bug for tracking changes to the about page
- [[25642]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25642) Technical notes are missing from the release

### Acquisitions

- [[10921]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=10921) You can edit an order even when it is in a closed basket
- [[11176]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11176) Purchase suggestions should respect the 'active' switch on budgets
- [[17458]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17458) When receiving an order, information about user and date on top are incorrect
- [[21268]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21268) Can't add to basket from staged file if base-level allocated is zero
- [[25266]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25266) Not all vendors are listed in the filters on the late order claims page
- [[25499]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25499) Fund code column is empty when closing a budget
- [[25507]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25507) PDF order print for German 2-pages is broken
- [[25545]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25545) Invoice page - Adjustments are not included in the Total + adjustments + shipment cost (Column tax. inc.)
- [[25599]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25599) Allow use of cataloguing placeholders when ACQ framework is used creating new record (UseACQFrameworkForBiblioRecords)
- [[25611]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25611) Changing the vendor when creating the basket does not keep that new vendor
- [[25751]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25751) When an ORDERED suggestion is edited, the status resets to "No status"
- [[25887]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25887) Filtering funds by library resets to empty in library pull down
- [[26497]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26497) "Hide all columns" throws Javascript error on aqplan.pl

### Architecture, internals, and plumbing

- [[16357]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16357) Plack error logs are not time stamped
- [[21539]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21539) addorderiso2709.pl forces librarian to select a ccode and notforloan code when using MarcItemFieldsToOrder
- [[25360]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25360) Use secure flag for CGISESSID cookie
- [[25875]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25875) Patron displayed multiple times in add user search if they have multiple sub permissions
- [[25950]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25950) REMOTE_ADDR set to null if client_ip in X-Forwarded-For matches a koha_trusted_proxies value
- [[26228]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26228) Update gulpfile to work with Node.js v12
- [[26239]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26239) Number::Format issues with large negative numbers
- [[26270]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26270) XISBN.t is failing since today
- [[26331]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26331) svc/letters/preview is not executable which prevents CGI functioning
- [[26384]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26384) Missing test to catch for execution flags
- [[26464]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26464) Code correction in opac-main when news_id passed

### Cataloging

- [[15933]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15933) Add cataloguing plugin to search for existing publishers in other records
- [[24780]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24780) 952$i stocknumber does not display in batch item modification
- [[25189]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25189) AutoCreateAuthorities can repeatedly generate authority records when using Default linker
- [[25553]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25553) Edit item date sort does not sort correctly
- [[26139]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26139) 'Place hold' button isn't hidden in all detail views if there are no items available for loan
- [[26289]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26289) Deleting biblio in labeled MARC view doesn't work

### Circulation

- [[23695]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23695) Items holdingbranch should be set to the originating library when generating a manual transfer
- [[24279]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24279) Claims Returned does not work when set from moredetail.pl or additem.pl
- [[25293]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25293) Don't call escapeHtml on null
- [[25440]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25440) Remove undef and CGI warnings and fix template variables list in circulation rules
- [[25584]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25584) When a 'return claim' is added, the button disappears, but the claim date doesn't show up
- [[25587]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25587) JavaScript issue - "clear" button doesn't reset some dropdowns
- [[25658]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25658) Print icon sometimes obscures patron barcode
- [[25724]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25724) Transferred item checked in to shelving cart has cart location removed when transfer is filled
- [[25807]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25807) Version 3.008 of Template breaks smart-rules display
- [[25868]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25868) Transfers page must show effective itemtype
- [[25890]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25890) Checkouts table not sorting on check out date correctly
- [[25940]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25940) Two separate print dialogs when checking in/transferring an item
- [[25958]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25958) Allow LongOverdue cron to exclude specified lost values
- [[26012]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26012) Date in 'Paid for' information not formatted to Time/DateFormat system preferences
- [[26076]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26076) Paying selected accountlines in full may result in the error "You must pay a value less than or equal to $x"
- [[26136]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26136) Prevent double submit of checkin form
- [[26224]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26224) Prevent double submit of header checkin form
- [[26323]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26323) Not_for_loan, damaged, location and ccode values must be retrieved from the correct AV category
- [[26362]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26362) Overdue report shows incorrect branches for patron, holdingbranch, and homebranch
- [[26424]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26424) Better performance of svc/checkouts

### Command-line Utilities

- [[22470]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22470) Missing the table name on misc/migration_tools/switch_marc21_series_info.pl
- [[25624]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25624) Update patrons category script should allow finding null and not null and wildcards
- [[25853]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25853) update_patrons_category.pl has incorrect permissions in repo
- [[25955]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25955) compare_es_to_db.pl broken by drop of Catmandu dependency
- [[26407]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26407) fix query in 'title exists' in `search_for_data_inconsistencies.pl`
- [[26448]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26448) koha-elasticsearch --commit parameter is not used
- [[26451]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26451) Small typo in bulkmarcimport.pl

### Documentation

- [[25576]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25576) ILL requests Help does not take you to the correct place in the manual

### Fines and fees

- [[26189]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26189) Table options on points of sale misaligned
- [[26541]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26541) Apply discount button misleading

### Hold requests

- [[23485]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23485) Holds to pull (pendingreserves.pl) should list barcodes
- [[25555]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25555) Holds Queue sorts patrons by firstname
- [[25789]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25789) New expiration date on placing a hold in staff interface can be set to a date in the past

  **Sponsored by** *Koha-Suomi Oy*
- [[26460]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26460) Wrong line ending (semicolon vs comma) in request.tt

### I18N/L10N

- [[25346]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25346) Only show warn about existing directory on installing translations when verbose is used
- [[25626]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25626) Translation issues with OPAC problem reports (status and 'sent to')
- [[26418]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26418) The "description" for REFUND accountlines is not translatable

### Installation and upgrade (web-based installer)

- [[25448]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25448) Update German (de-DE) framework files
- [[25491]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25491) Perl warning at the login page of installer
- [[25695]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25695) Missing logging of $@ in onboarding.pl after eval block

### MARC Bibliographic data support

- [[25701]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25701) Facets display in random order

### Notices

- [[25629]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25629) Fix capitalization in sample notices

### OPAC

- [[11994]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11994) Fix OpenSearch discovery in the OPAC

  >OpenSearch (https://en.wikipedia.org/wiki/OpenSearch) allows you to search your library's catalog directly from the browser address bar or search box. This fixes the OpenSearch feature so that it now works correctly in Firefox. Note: make sure OPACBaseURL is correctly set.
- [[20783]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20783) Cannot embed some YouTube videos due to 403 errors
- [[23276]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23276) Don't show tags on tag cloud when tagging is disabled
- [[24352]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24352) Wrong labels displaying in result list with OpacItemLocation

  >This fixes the OPAC's MARC21 search results XSLT so that OPAC search result information is correctly labelled based on the OpacItemLocation preference.
  >
  >Previously, search results showed the label "Location(s)" whether the
  >setting was "collection code" or "location."
- [[24473]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24473) Syndetics content should be $raw filtered on opac-detail.tt

  >Syndetics provides enhanced content which is displayed in the OPAC under the tabs 'Title Notes', 'Excerpt', 'About the author', and 'Editions'. They provide this information as HTML but Koha currently displays the content with the HTML tags. This fixes this so that the enhanced content displays correctly.
- [[25434]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25434) When viewing cart on small screen sizes selections-toolbar is hidden
- [[25597]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25597) Javascript errors in self-checkout printslip.pl preventing printing
- [[25869]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25869) Coce images not loading for lists (virtualshelves)
- [[25914]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25914) Relative's checkouts have empty title in OPAC
- [[25982]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25982) OPAC shelves RSS link output is HTML not XML
- [[26070]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26070) Google Transliterate API has been deprecated

  >The Google Transliterate API has been deprecated by Google in 2011. This removes the remaining code and GoogleIndicTransliteration system preference from Koha as this is no longer functional.
- [[26119]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26119) Patron attribute option to display in OPAC is not compatible with PatronSelfRegistrationVerifyByEmail
- [[26127]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26127) When reporting an Error from the OPAC, the URL does not display
- [[26179]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26179) Remove redundant import of Google font
- [[26262]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26262) Paging on course reserves tables in OPAC is broken
- [[26388]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26388) Renew all and Renew selected buttons should account for items that can't be renewed
- [[26421]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26421) Use Bootstrap screen reader text class for shelf browser messages
- [[26478]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26478) Display issue with buttons on the self checkout screens
- [[26512]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26512) Display issue with buttons for OPAC checkout note

### Packaging

- [[25509]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25509) Remove useless libjs-jquery dependency
- [[25778]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25778) koha-plack puts duplicate entries into PERL5LIB when multiple instances named
- [[25889]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25889) Increase performance of debian/list-deps script

### Patrons

- [[25336]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25336) Show checkouts/fines to guarantor is in the wrong section of the patron file
- [[26125]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26125) In 'Patron search' tab link should lead to patron details instead of checkout screen

### Plugin architecture

- [[25953]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25953) Add ID to installed plugins table to ease styling and DOM mods

### REST API

- [[25570]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25570) Listing requests should be paginated by default
- [[25662]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25662) Create hold route does not check maxreserves syspref
- [[26271]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26271) Call to /api/v1/patrons/<patron_id>/account returns 500 error if manager_id is NULL

### Reports

- [[17801]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17801) 'Top Most-circulated items' gives wrong results when filtering by checkout date
- [[26111]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26111) Serials module does not appear in reports dictionary
- [[26165]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26165) Duplicating large saved report leads to error due to length of URI

### SIP2

- [[25805]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25805) SIP will show hold patron name (DA) as something like C4::SIP::SIPServer=HASH(0x88175c8) if there is no patron
- [[25903]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25903) Sending a SIP patron information request with a summary field flag in indexes 6-9 will crash server

### Searching

- [[17661]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17661) Differences in field ending (whitespace, punctuation) cause duplicate facets

### Searching - Elasticsearch

- [[24807]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24807) Add "year" type to improve sorting by publication date
- [[25872]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25872) Advanced search on OPAC with limiter but no search term fails when re-sorted
- [[25873]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25873) Elasticsearch - Records with malformed data are not indexed
- [[26009]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26009) Elasticsearch homebranch and holdingbranch facets are limited to 10
- [[26313]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26313) "Show analytics" and "Show volumes" links don't work with Elasticsearch and UseControlNumber

### Self checkout

- [[25349]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25349) Enter in the username field submits the login, instead of moving focus to the password field
- [[25791]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25791) SCO print dialog pops up twice
- [[26131]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26131) console errors when attempting to open SCO related system preferences
- [[26301]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26301) Self-checkout blocks renew for overdues even when OverduesBlockRenewing allows it in opac-user.pl

### Serials

- [[25696]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25696) Test prediction pattern button is invalid HTML

### Staff Client

- [[11223]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11223) Incorrect ind 1 semantics for MARC21 785 on the detail page in staff
- [[25521]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25521) NewItemsDefaultLocation description should not mention cart_to_shelf.pl
- [[25537]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25537) Page reload at branchtransfers.pl loses destination branch
- [[25744]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25744) Replace <i> tags with <em> AND <b> tags with <strong> in the staff interface
- [[25756]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25756) Empty HTML table row after OPAC "Appearance" preferences
- [[25804]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25804) Remove HTML from title tag of bibliographic detail page
- [[26084]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26084) ConsiderOnSiteCheckoutsAsNormalCheckouts description is unclear
- [[26249]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26249) keep_text class not set inconsistently in cat-search.inc
- [[26435]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26435) AutoSelfCheckID syspref description should warn it blocks OPAC access

### System Administration

- [[25394]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25394) Cronjob path in the AuthorityMergeLimit syspref description is wrong

  >Updates the system preference description with the correct path for the cronjob (misc/cronjobs/merge_authorities.pl).
- [[25675]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25675) System preference PatronSelfRegistration incorrectly described
- [[25919]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25919) Desks link is available in left side menu even if UseCirculationDesks is disabled
- [[26283]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26283) dateexpiry and dateenrolled are missing in the new modal for BorrowerMandatoryField and others

  >This enhancement adds the dateenrolled and dateexpiry fields to the list of fields that can be selected in system preferences such as the BorrowerMandatoryField.
- [[26490]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26490) Column configuration for account-fines hides the wrong columns

### Templates

- [[25447]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25447) Terminology: Fix button text "Edit biblio"

  >This updates the text on the cataloging main page so that in the menu for each search result the "Edit biblio" link is now "Edit record."
- [[25582]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25582) Don't show OPAC problems entry on dashboard when there are no reports
- [[25615]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25615) Empty select in "Holds to pull" filters
- [[25718]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25718) Correct typo in additem.tt
- [[25747]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25747) Don't display a comma when patron has no firstname
- [[25762]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25762) Typo in linkitem.tt
- [[25765]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25765) Replace LoginBranchname and LoginBranchcode with use of Branches template plugin
- [[25896]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25896) Missing closing </td> tag in smart-rules.tt
- [[25974]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25974) Remove inline style from table settings administration page
- [[25987]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25987) Radio buttons are misaligned in New label batches
- [[26049]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26049) Replace li with span class results_summary in UNIMARC intranet XSLT
- [[26093]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26093) Markup error after Bug 24279 creates formatting problem
- [[26098]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26098) JS error on the fund list view if no fund displayed
- [[26213]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26213) Remove the use of jquery.checkboxes plugin when adding orders from MARC file
- [[26234]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26234) Default DataTables must know our own classes
- [[26324]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26324) Spelling error resizeable vs resizable

### Test Suite

- [[24147]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24147) Objects.t is failing randomly
- [[25623]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25623) Some tests in oauth.t do not roll back
- [[25638]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25638) API related tests failing on comparing floats
- [[25641]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25641) Koha/XSLT/Base.t is failing on U20
- [[25729]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25729) Charges/Fees.t is failing on slow servers due to wrong date comparison
- [[25811]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25811) authentication.t is failing randomly
- [[26043]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26043) Holds.t is failing randomly
- [[26115]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26115) Remove leftover Carp::Always
- [[26162]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26162) Prevent Selenium's StaleElementReferenceException
- [[26365]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26365) Koha/Acquisition/Order.t is failing with MySQL 8
- [[26401]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26401) xt/fix-old-fsf-address* are no longer needed

### Tools

- [[25862]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25862) TinyMCE editor mangles  local url links  (relative_urls is true) in tools/koha-new.pl
- [[25893]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25893) Log viewer no longer searches using wildcards
- [[26017]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26017) Cashup registers never shows on tools page
- [[26121]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26121) When using CodeMirror in News Tool DatePicker is hard to see
- [[26124]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26124) Console errors on tools_koha-news when editing with TinyMCE
- [[26236]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26236) log viewer does not translate the interface properly
- [[26414]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26414) Unable to export Withdrawn status using CSV profile

  >This patch fixes the export of MARC records and the withdrawn status when using CSV profiles. Before this fix the full 952 field was exported, rather than just the withdrawn status.

### Web services

- [[25793]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25793) OAI 'Set' and 'Metadata' dropdowns broken by OPAC jQuery upgrade

### Z39.50 / SRU / OpenSearch Servers

- [[25702]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25702) Actions button on Search results from Z39.50 is displayed incorrectly
## New sysprefs

- AddressForFailedOverdueNotices
- AutoCreditNumber
- BiblioItemtypeInfo
- CircConfirmItemParts
- EdifactInvoiceImport
- ILLHiddenRequestStatuses
- NoRefundOnLostReturnedItemsAge
- OpacMetaDescription
- PatronDuplicateMatchingAddFields
- Pseudonymization
- PseudonymizationPatronFields
- PseudonymizationTransactionFields

## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.05/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (100%)
- Armenian (100%)
- Armenian (Classical) (99.7%)
- Chinese (Taiwan) (94.1%)
- Czech (81.6%)
- English (New Zealand) (67.7%)
- English (USA)
- Finnish (70.4%)
- French (82.8%)
- French (Canada) (96%)
- German (97.7%)
- German (Switzerland) (75.5%)
- Greek (61.8%)
- Hindi (100%)
- Italian (83.1%)
- Norwegian Bokmål (72.2%)
- Polish (74%)
- Portuguese (88.3%)
- Portuguese (Brazil) (99.4%)
- Slovak (89.1%)
- Spanish (100%)
- Swedish (78.7%)
- Telugu (90.9%)
- Turkish (95.1%)
- Ukrainian (66.4%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.05.04 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits
We thank the following libraries who are known to have sponsored
new features in Koha 20.05.04:

- [Association KohaLa](https://koha-fr.org/)
- [ByWater Solutions](https://bywatersolutions.com/)
- [Catalyst](https://www.catalyst.net.nz/products/library-management-koha)
- Cooperative Information Network (CIN)
- Galway-Mayo Institute of Technology
- Horowhenua District Council
- Institute of Technology Carlow
- Institute of Technology Tallaght
- Koha-Suomi Oy
- National Library of Finland
- Northeast Kansas Library - NEKLS
- Orex Digital
- [PTFS Europe](https://ptfs-europe.com/)
- [Royal College of Music](https://www.rcm.ac.uk/)
- [South East Kansas Library System](http://www.sekls.org)

We thank the following individuals who contributed patches to Koha 20.05.04.

- Aleisha Amohia (3)
- Tomás Cohen Arazi (87)
- Alex Buckley (3)
- Colin Campbell (2)
- Nick Clemens (103)
- David Cook (20)
- Chris Cormack (1)
- Frédéric Demians (1)
- Jonathan Druart (398)
- Magnus Enger (2)
- Katrin Fischer (72)
- Andrew Fuerste-Henry (9)
- Lucas Gass (14)
- Didier Gautheron (5)
- Caitlin Goodger (1)
- David Gustafsson (6)
- Kyle Hall (64)
- Mason James (10)
- Andreas Jonsson (1)
- Olli-Antti Kivilahti (2)
- Bernardo González Kriegel (3)
- Joonas Kylmälä (9)
- Nicolas Legrand (2)
- Owen Leonard (145)
- Ere Maijala (1)
- Hayley Mapley (2)
- Julian Maurice (20)
- Matthias Meusburger (1)
- Josef Moravec (13)
- Agustín Moyano (20)
- David Nind (2)
- Andrew Nugged (11)
- Liz Rea (1)
- Martin Renvoize (139)
- Alexis Ripetti (1)
- David Roberts (4)
- Marcel de Rooy (34)
- Caroline Cyr La Rose (3)
- Andreas Roussos (2)
- Slava Shishkin (3)
- Joe Sikowitz (2)
- Fridolin Somers (14)
- Emmi Takkinen (14)
- Lari Taskula (2)
- Petro Vashchuk (5)
- Timothy Alexis Vass (3)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.05.04

- Athens County Public Libraries (145)
- BibLibre (40)
- Bibliotheksservice-Zentrum Baden-Württemberg (BSZ) (72)
- Bibliothèque Universitaire des Langues et Civilisations (BULAC) (2)
- ByWater-Solutions (190)
- Catalyst (7)
- Dataly Tech (2)
- David Nind (2)
- Fenway Library Organization (2)
- Göteborgs Universitet (2)
- Hypernova Oy (3)
- Independant Individuals (53)
- Koha Community Developers (399)
- KohaAloha (10)
- Kreablo AB (1)
- Libriotech (2)
- Prosentient Systems (20)
- PTFS-Europe (145)
- Rijks Museum (34)
- Solutions inLibro inc (4)
- Tamil (1)
- Theke Solutions (107)
- ub.lu.se (3)
- Universidad Nacional de Córdoba (3)
- University of Helsinki (10)
- Wellington East Girls' College (1)

We also especially thank the following individuals who tested patches
for Koha.

- Marco Abi-Ramia (2)
- Hugo Agud (9)
- Aleisha Amohia (1)
- Tomás Cohen Arazi (99)
- Alex Arnaud (44)
- Donna Bachowski (2)
- barbara (1)
- Marjorie Barry-Vila (2)
- Bob Bennhoff (6)
- Henry Bolshaw (4)
- Signed-off-by: Sonia Bouis (20)
- Christopher Brannon (12)
- Alex Buckley (2)
- Nick Clemens (85)
- Rebecca Coert (7)
- David Cook (23)
- Holly Cooper (4)
- Sarah Cornell (1)
- Frédéric Demians (6)
- Michal Denar (9)
- donnab (1)
- Jonathan Druart (916)
- Magnus Enger (2)
- Bouzid Fergani (2)
- Katrin Fischer (476)
- Andrew Fuerste-Henry (37)
- Daniel Gaghan (3)
- Jeff Gaines (1)
- Bonnie Gardner (1)
- Lucas Gass (18)
- Didier Gautheron (8)
- Claire Gravely (1)
- Victor Grousset (28)
- Amit Gupta (18)
- Kyle Hall (94)
- Stina Hallin (3)
- Sally Healey (49)
- Heather Hernandez (2)
- Abbey Holt (1)
- Andrew Isherwood (1)
- Brandon J (7)
- Jennifer (1)
- Barbara Johnson (1)
- Pasi Kallinen (2)
- Katrin (1)
- Jill Kleven (1)
- Bernardo González Kriegel (5)
- Rhonda Kuiper (1)
- Joonas Kylmälä (31)
- Peter Lau (1)
- Nicolas Legrand (1)
- Owen Leonard (68)
- Ere Maijala (1)
- Hayley Mapley (6)
- Jesse Maseto (1)
- Julian Maurice (29)
- Kelly McElligott (13)
- Josef Moravec (17)
- Agustín Moyano (8)
- David Nind (120)
- Kim Peine (3)
- Emma Perks (8)
- Simon Perry (9)
- Séverine Queune (4)
- Liz Rea (14)
- Martin Renvoize (216)
- Alexis Ripetti (6)
- Jason Robb (4)
- Marcel de Rooy (88)
- Caroline Cyr La Rose (5)
- Andreas Roussos (1)
- Lisette Scheer (18)
- Maryse Simard (1)
- Fridolin Somers (8)
- Michael Springer (1)
- Debi Stears (1)
- Myka Kennedy Stephens (1)
- Deb Stephenson (1)
- Arthur Suzuki (5)
- Emmi Takkinen (4)
- Timothy Alexis Vass (3)
- Mengü Yazıcıoğlu (2)
- Jessie Zairo (1)
- Jessica Zairo (1)
- Christofer Zorn (2)

We thank the following individuals who mentored new contributors to the Koha project.

- Andrew Nugged


We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is master.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 08 Oct 2020 09:34:44.
