# RELEASE NOTES FOR KOHA 20.05.06
19 Dec 2020

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.05.06 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.05.06.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.05.06 is a bugfix/maintenance release.

It includes 2 enhancements, 37 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required




## Enhancements

### Architecture, internals, and plumbing

- [[27002]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27002) Make Koha::Biblio->pickup_locations return a Koha::Libraries resultset

### MARC Authority data support

- [[25313]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25313) Add optional skip_merge parameter to ModAuthority


## Critical bugs fixed

### Acquisitions

- [[18267]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18267) Update price and tax fields in EDI to reflect DB changes
- [[27082]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27082) Problem when a basket has more of 20 orders with uncertain price

### Cataloging

- [[26518]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26518) Adding a record can succeed even if adding the biblioitem fails

### Fines and fees

- [[26536]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26536) "Writeoff/Pay selected" deducts from old unpaid debts first
- [[27079]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27079) UpdateFine adds refunds for fines paid off before return

### MARC Bibliographic record staging/import

- [[26854]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26854) stage-marc-import.pl does not properly fork

### Patrons

- [[27144]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27144) Cannot delete any patrons

### Reports

- [[25942]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25942) Batch biblio and borrower operations on report results should not concatenate biblio/cardnumbers into a single string
- [[27142]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27142) Patron batch update from report module - no patrons loaded into view

  >This fixes an error when batch modifying patrons using the reports module. After running a report (such as SELECT * FROM borrowers LIMIT 50) and selecting batch modification an error was displayed: "Warning, the following cardnumbers were not found:", and you were not able to modify any patrons.

### SIP2

- [[27166]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27166) SIP2 Connection is killed when an item that was not issued is checked in and generates a transfer

### Searching

- [[7607]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=7607) Advanced search: Index and search term don't match when leaving fields empty

### Searching - Elasticsearch

- [[26903]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26903) Authority records not being indexed in Elasticsearch
- [[27070]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27070) Elasticsearch - with Elasticsearch 6 searches failing unless all terms are in the same field

### Searching - Zebra

- [[12430]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=12430) Relevance ranking should also be used without QueryWeightFields system preference

### Serials

- [[26992]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26992) On serial collection page, impossible to delete issues and related items

### Tools

- [[26516]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26516) Importing records with unexpected format of copyrightdate fails


## Other bugs fixed

### About

- [[27108]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27108) Update team for 21.05 cycle

### Acquisitions

- [[26905]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26905) Purchase suggestion button hidden for users with suggestion permission but not acq permission

### Architecture, internals, and plumbing

- [[16067]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16067) Koha::Cache, fastmmap caching system is broken
- [[27021]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27021) Chaining on Koha::Objects->empty should always return an empty resultset
- [[27092]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27092) Remove note about "synced mirror" from the README.md

### Circulation

- [[25583]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25583) When ClaimReturnedLostValue is not set, the claim returned tab doesn't appear

### Command-line Utilities

- [[26337]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26337) Remove unused authorities script should skip merge

### Hold requests

- [[26976]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26976) When renewalsallowed is empty the UI is not correct
- [[26988]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26988) Defer loading the hold pickup locations until the dropdown is selected

### Packaging

- [[25548]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25548) Package install Apache performs unnecessary redirects

### Patrons

- [[14708]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14708) The patron set as the anonymous patron should not be deletable

### Serials

- [[26846]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26846) serial-collections page should select the expected and late serials automatically

### Staff Client

- [[23475]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23475) Search context is lost when simple search leads to a single record
- [[26938]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26938) Prevent flash of unstyled content on sales table
- [[26944]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26944) Help link from automatic item modification by age should go to the relevant part of the manual
- [[26946]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26946) Limit size of cash register's name on the UI

### Test Suite

- [[25514]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25514) REST/Plugin/Objects.t is failing randomly
- [[26984]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26984) Tests are failing if AnonymousPatron is configured
- [[26986]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26986) Second try to prevent Selenium's StaleElementReferenceException
- [[27007]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27007) GetMarcSubfieldStructure called with "unsafe" in tests

### Tools

- [[26336]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26336) Cannot import items if items ignored when staging


## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.05/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (99.1%)
- Armenian (100%)
- Armenian (Classical) (99.7%)
- Chinese (Taiwan) (94.5%)
- Czech (81%)
- English (New Zealand) (67%)
- English (USA)
- Finnish (70.8%)
- French (82%)
- French (Canada) (96.7%)
- German (100%)
- German (Switzerland) (74.8%)
- Greek (62.3%)
- Hindi (100%)
- Italian (100%)
- Norwegian Bokmål (71.4%)
- Polish (73.8%)
- Portuguese (87.3%)
- Portuguese (Brazil) (98.4%)
- Russian (71.9%)
- Slovak (90.1%)
- Spanish (99.9%)
- Swedish (80%)
- Telugu (90%)
- Turkish (100%)
- Ukrainian (66.5%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.05.06 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits

We thank the following individuals who contributed patches to Koha 20.05.06.

- Tomás Cohen Arazi (8)
- Philippe Blouin (1)
- Colin Campbell (1)
- Nick Clemens (21)
- David Cook (4)
- Frédéric Demians (1)
- Jonathan Druart (19)
- Andrew Fuerste-Henry (2)
- Victor Grousset (1)
- Kyle M Hall (8)
- Joonas Kylmälä (1)
- Owen Leonard (1)
- Agustín Moyano (1)
- Martin Renvoize (10)
- Marcel de Rooy (1)
- Fridolin Somers (1)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.05.06

- Athens County Public Libraries (1)
- BibLibre (1)
- ByWater-Solutions (31)
- Koha Community Developers (20)
- Prosentient Systems (4)
- PTFS-Europe (11)
- Rijks Museum (1)
- Solutions inLibro inc (1)
- Tamil (1)
- Theke Solutions (9)
- University of Helsinki (1)

We also especially thank the following individuals who tested patches
for Koha.

- Tomás Cohen Arazi (10)
- Bob Bennhoff (1)
- Nick Clemens (6)
- David Cook (1)
- Jonathan Druart (60)
- Katrin Fischer (10)
- Andrew Fuerste-Henry (86)
- Lucas Gass (1)
- Victor Grousset (22)
- Kyle M Hall (4)
- Sally Healey (5)
- Joonas Kylmälä (1)
- Julian Maurice (3)
- Josef Moravec (10)
- David Nind (16)
- Martin Renvoize (19)
- Phil Ringnalda (1)
- Fridolin Somers (15)
- Timothy Alexis Vass (1)
- Mengü Yazıcıoğlu (1)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.05.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 19 Dec 2020 09:48:03.
