# RELEASE NOTES FOR KOHA 18.11.07
19 Jul 2019

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 18.11.07 can be downloaded from:

- [Download](http://download.koha-community.org/koha-18.11.07.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 18.11.07 is a bugfix/maintenance release.

It includes 5 bugfixes.






## Critical bugs fixed

### Patrons

- [[23283]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23283) cannot view/edit additional attributes in 18.11.x


## Other bugs fixed

### Architecture, internals, and plumbing

- [[23144]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23144) Bad POD breaks svc/barcode

### Self checkout

- [[23198]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23198) [18.11] Wrong icons/paths in 18.11 for self check modules

### Serials

- [[23065]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=23065) 'New subscription' button in serials sometimes uses a blank form and sometimes defaults to current serial

### Templates

- [[22851]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22851) Navigation links in the serials module should be styled the same as other modules



## System requirements

Important notes:
    
- Perl 5.10 is required
- Zebra is required

## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/18.11/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (98.4%)
- Armenian (100%)
- Basque (65.9%)
- Chinese (China) (64%)
- Chinese (Taiwan) (99.6%)
- Czech (93.6%)
- Danish (55.3%)
- English (New Zealand) (88.2%)
- English (USA)
- Finnish (84.4%)
- French (99.3%)
- French (Canada) (99.6%)
- German (100%)
- German (Switzerland) (91.7%)
- Greek (78.6%)
- Hindi (100%)
- Italian (93.8%)
- Norwegian Bokmål (94.8%)
- Occitan (post 1500) (59.5%)
- Polish (86.7%)
- Portuguese (100%)
- Portuguese (Brazil) (87.5%)
- Slovak (90.1%)
- Spanish (100%)
- Swedish (90.7%)
- Turkish (98.3%)
- Ukrainian (62.2%)
- Vietnamese (54.6%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 18.11.07 is

- Release Manager: Martin Renvoize
- Release Manager assistants:
  - Tomás Cohen Arazi
  - Nick Clemens
- QA Manager: Katrin Fischer
- QA Team:
  - Tomás Cohen Arazi
  - Alex Arnaud
  - Nick Clemens
  - Jonathan Druart
  - Kyle Hall
  - Julian Maurice
  - Josef Moravec
  - Marcel de Rooy
- Topic Experts:
  - REST API -- Tomás Cohen Arazi
  - SIP2 -- Kyle Hall
  - UI Design -- Owen Leonard
  - Elasticsearch -- Alex Arnaud
  - ILS-DI -- Arthur Suzuki
  - Authentication -- Martin Renvoize
- Bug Wranglers:
  - Michal Denár
  - Indranil Das Gupta
  - Jon Knight
  - Lisette Scheer
  - Arthur Suzuki
- Packaging Manager: Mirko Tietgen
- Documentation Manager: David Nind
- Documentation Team:
  - Andy Boze
  - Caroline Cyr-La-Rose
  - Lucy Vaux-Harvey

- Translation Managers: 
  - Indranil Das Gupta
  - Bernardo González Kriegel
- Release Maintainers:
  - 19.05 -- Fridolin Somers
  - 18.11 -- Lucas Gass
  - 18.05 -- Liz Rea
## Credits

We thank the following individuals who contributed patches to Koha 18.11.07.

- Nick Clemens (1)
- Jonathan Druart (1)
- Lucas Gass (2)
- Owen Leonard (2)
- Julian Maurice (1)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 18.11.07

- ACPL (2)
- BibLibre (1)
- ByWater-Solutions (3)
- Koha Community Developers (1)

We also especially thank the following individuals who tested patches
for Koha.

- Nick Clemens (1)
- Chris Cormack (1)
- Michal Denar (1)
- Katrin Fischer (2)
- Lucas Gass (6)
- Claire Gravely (1)
- Kyle Hall (1)
- Martin (1)
- Julian Maurice (2)
- Martin Renvoize (4)
- Fridolin Somers (4)
- Mark Tompsett (1)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 18.11.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 19 Jul 2019 09:19:39.
