# RELEASE NOTES FOR KOHA 20.11.06
16 Jun 2021

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.11.06 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.11.06.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.11.06 is a bugfix/maintenance release.

It includes 1 enhancements, 6 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required




## Enhancements

### Staff Client

- [[28091]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28091) Add meta tag with Koha version number to staff interface pages


## Critical bugs fixed

### Architecture, internals, and plumbing

- [[28200]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28200) Net::Netmask 1.9104-2 requires constructor change for backwards compatibility

  >The code library Koha uses for working with IP addresses has dropped support for abbreviated values in recent releases.  This is to tighten up the default security of input value's and we have opted in Koha to follow this change through into our system preferences for the same reason.
  >
  >WARNING: `koha_trusted_proxies` and `ILS-DI:AuthorizedIPs` are both affected. Please check that you are not using abbreviated IP forms for either of these cases. Example: "10.10" is much less explicit than "10.10.0.0/16" and should be avoided.

### I18N/L10N

- [[28419]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28419) Page addorderiso2709.pl is untranslatable

### Packaging

- [[28364]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28364) koha-z3950-responder breaks because of log4perl.conf permissions

### Searching

- [[28475]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28475) Searching all headings returns no results

  **Sponsored by** *Asociación Latinoamericana de Integración*

### Tools

- [[28158]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=28158) Lost items not charging when marked lost from batch item modification


## Other bugs fixed

### Templates

- [[27899]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27899) Missing description for libraryNotPickupLocation on request.pl


## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.11/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (99.5%)
- Armenian (100%)
- Armenian (Classical) (89%)
- Bulgarian (61%)
- Catalan; Valencian (54.8%)
- Chinese (Taiwan) (91.7%)
- Czech (73%)
- English (New Zealand) (59.6%)
- English (USA)
- Finnish (79.3%)
- French (89.7%)
- French (Canada) (91.1%)
- German (100%)
- German (Switzerland) (66.9%)
- Greek (60.7%)
- Hindi (100%)
- Italian (100%)
- Nederlands-Nederland (Dutch-The Netherlands) (72.9%)
- Norwegian Bokmål (63.8%)
- Polish (92%)
- Portuguese (88.5%)
- Portuguese (Brazil) (95.9%)
- Russian (94%)
- Slovak (80.7%)
- Spanish (99.2%)
- Swedish (74.8%)
- Telugu (100%)
- Turkish (100%)
- Ukrainian (66.7%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.11.06 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits
We thank the following libraries who are known to have sponsored
new features in Koha 20.11.06:

- Asociación Latinoamericana de Integración

We thank the following individuals who contributed patches to Koha 20.11.06.

- Tomás Cohen Arazi (2)
- David Cook (1)
- Jonathan Druart (5)
- Lucas Gass (1)
- Kyle M Hall (1)
- Owen Leonard (1)
- Fridolin Somers (3)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.11.06

- Athens County Public Libraries (1)
- BibLibre (3)
- ByWater-Solutions (2)
- Koha Community Developers (5)
- Prosentient Systems (1)
- Theke Solutions (2)

We also especially thank the following individuals who tested patches
for Koha.

- Tomás Cohen Arazi (1)
- Nick Clemens (3)
- Alvaro Cornejo (1)
- Jonathan Druart (6)
- Lucas Gass (1)
- Victor Grousset (6)
- Kyle M Hall (1)
- Ere Maijala (1)
- David Nind (1)
- Martin Renvoize (4)
- Fridolin Somers (11)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.11.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 16 Jun 2021 13:09:50.
