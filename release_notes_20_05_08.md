# RELEASE NOTES FOR KOHA 20.05.07
15 Jan 2021

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.05.07 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.05.07.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.05.07 is a bugfix/maintenance release.

It includes 3 enhancements, 14 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required




## Enhancements

### Label/patron card printing

- [[26875]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26875) Allow printing of just one barcode

### Searching - Elasticsearch

- [[24863]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24863) QueryFuzzy syspref says it requires Zebra but Elasticsearch has some support

### Staff Client

- [[25462]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25462) Shelving location should be on a new line in holdings table

  >In the holdings table, the shelving location is now displayed on a new line after the 'Home library'.


## Critical bugs fixed

### Command-line Utilities

- [[27245]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27245) bulkmarcimport.pl error 'Already in a transaction'

### Database

- [[25826]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25826) Hiding biblionumber in the frameworks breaks links in result list


## Other bugs fixed

### Cataloging

- [[26330]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26330) jQueryUI tabs don't work with non-Latin-1 characters
- [[27137]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27137) Move item doesn't show the title of the target record

  **Sponsored by** *Toi Ohomai Institute of Technology*

  >This patch fixes a small bug to ensure that the title of the target bibliographic record shows as expected upon successfully attaching an item.

### Command-line Utilities

- [[27085]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27085) Corrections in overdue_notices.pl help text

  **Sponsored by** *Lunds Universitetsbibliotek*

### Hold requests

- [[26698]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26698) Hold can show as waiting and in transit at the same time

### MARC Bibliographic record staging/import

- [[26171]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26171) Show biblionumber in Koha::Exceptions::Metadata::Invalid

### OPAC

- [[26397]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26397) opac.scss calls non-existent image

  >This patch removes some obsolete CSS from the OPAC which calls a non-existent image.
- [[27178]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27178) OPAC results and lists pages contain invalid attributes (xmlns:str="http://exslt.org/strings")

### Searching - Elasticsearch

- [[26996]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26996) Elasticsearch: Multiprocess reindexing sometimes doesn't reindex all records

  **Sponsored by** *Lund University Library*
- [[27043]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27043) Add to number_of_replicas and number_of_shards  to index config

  >Elasticsearch 6 server has default value 5 for "number_of_shards" but warn about Elasticsearch 7 having default value 1.
  >So its is better to set this value in configuration file.
  >Patch also sets number_of_replicas to 1.
  >If you have only one Elasticsearch node, you have to set this value to 0.

### System Administration

- [[27280]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27280) Explanation for "Days mode" is not consistent

### Templates

- [[25954]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25954) Header search forms should be labeled

### Test Suite

- [[26364]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26364) XISBN.t makes a bad assumption about return values


## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.05/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (99.1%)
- Armenian (100%)
- Armenian (Classical) (99.7%)
- Chinese (Taiwan) (94.5%)
- Czech (81%)
- English (New Zealand) (67%)
- English (USA)
- Finnish (70.8%)
- French (81.9%)
- French (Canada) (96.8%)
- German (100%)
- German (Switzerland) (74.7%)
- Greek (62.3%)
- Hindi (100%)
- Italian (100%)
- Norwegian Bokmål (71.4%)
- Polish (73.8%)
- Portuguese (87.2%)
- Portuguese (Brazil) (98.4%)
- Russian (82.6%)
- Slovak (90.1%)
- Spanish (100%)
- Swedish (80%)
- Telugu (90%)
- Turkish (100%)
- Ukrainian (66.6%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.05.07 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits
We thank the following libraries who are known to have sponsored
new features in Koha 20.05.07:

- Lund University Library
- Lunds Universitetsbibliotek
- Toi Ohomai Institute of Technology

We thank the following individuals who contributed patches to Koha 20.05.07.

- Aleisha Amohia (1)
- Tomás Cohen Arazi (1)
- Nick Clemens (8)
- Jonathan Druart (6)
- Andrew Fuerste-Henry (4)
- Victor Grousset (1)
- Owen Leonard (6)
- Josef Moravec (1)
- Björn Nylén (1)
- Martin Renvoize (1)
- David Roberts (1)
- Lisette Scheer (1)
- Fridolin Somers (3)
- Timothy Alexis Vass (1)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.05.07

- Athens County Public Libraries (6)
- BibLibre (3)
- ByWater-Solutions (12)
- Independant Individuals (2)
- Koha Community Developers (7)
- Latah County Library District (1)
- PTFS-Europe (2)
- Theke Solutions (1)
- ub.lu.se (2)

We also especially thank the following individuals who tested patches
for Koha.

- Tomás Cohen Arazi (3)
- Nick Clemens (15)
- Jonathan Druart (21)
- Katrin Fischer (5)
- Andrew Fuerste-Henry (32)
- Victor Grousset (6)
- Joonas Kylmälä (1)
- Owen Leonard (1)
- Julian Maurice (1)
- Josef Moravec (2)
- David Nind (8)
- Martin Renvoize (6)
- Caroline Cyr La Rose (1)
- Fridolin Somers (25)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.05.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 15 Jan 2021 11:36:22.
